package pl.sda.dentystaRejestracja;

import java.time.LocalDate;
import java.util.Scanner;

public class PanelRezerwacji {

    public boolean isWorking = true;
    public Kalendarz kalendarz=new Kalendarz();


    public void przeprowadzRezerwacje() {
        Scanner input = new Scanner(System.in);
        while (isWorking) {
            System.out.println("WITAMY U DENTYSTY");
            System.out.println("Czy chcesz dokonac rezerwacji wizyty?");
            String odp1 = input.nextLine();
            if (odp1.toLowerCase().contains("tak")) {
                Pacjent pacjent = zgromadzDanePacjenta();
                System.out.println("Proszę podać datę wizyty");
                System.out.println("Podaj rok");
                String rok = input.nextLine();
                System.out.println("Podaj miesiac");
                String miesiac = input.nextLine();
                System.out.println("Podaj dzien");
                String dzien = input.nextLine();
                System.out.println("Podaj godzine");
                String godzina = input.nextLine();

                kalendarz.sprawdzDate(rok, miesiac, dzien);


            }else{
                System.out.println("Do wdzenia!");
                break;
            }
        }


    }

    public Pacjent zgromadzDanePacjenta(){
        Scanner input = new Scanner(System.in);
        Pacjent pacjent = new Pacjent();
        System.out.println("Podaj swoje imie");
        String imie = input.nextLine();
        pacjent.setImie(imie);
        System.out.println("Podaj swoje nazwisko");
        String nazwisko = input.nextLine();
        pacjent.setNazwisko(nazwisko);
        System.out.println("Podaj swoj wiek");
        int wiek = input.nextInt();
        pacjent.setWiek(wiek);
        System.out.println("Podaj krótki opis choroby");
        String g = input.nextLine();
        String opis = input.nextLine();
        pacjent.setOpisChoroby(opis);

        return pacjent;
    }
}
