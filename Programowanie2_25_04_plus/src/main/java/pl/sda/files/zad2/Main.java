package pl.sda.files.zad2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            PrintWriter printWriter = new PrintWriter("output_2.txt");
            String linia = input.nextLine();
            printWriter.println(linia);
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
