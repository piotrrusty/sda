package pl.sda.files;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isWorking = true;
        try (PrintWriter printWriter = new PrintWriter(new FileWriter("plik.txt", true))) {
            while (isWorking) {
                String linia = scanner.nextLine();
                printWriter.println(linia);
                printWriter.flush();
                if (linia.equals("quit")) {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}