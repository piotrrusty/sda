package pl.sda.files.zadBankByPawel;

public class KontoBankowe {
    private Double bilans;
    private final Object zamek = new Object();

    public KontoBankowe() {
        this.bilans = 0.0;
    }

    public synchronized void dodajDoKonta(double kwota) {
        synchronized (zamek) {
            bilans = bilans + kwota;
        }
    }

    public synchronized void odejmijOdKonta(double kwota){
        synchronized (zamek) {
            bilans = bilans - kwota;
        }
    }

    public Double getBilans(){
        return bilans;
    }
}
