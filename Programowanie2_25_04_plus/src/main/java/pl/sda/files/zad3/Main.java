package pl.sda.files.zad3;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean isWorking = true;
        try (PrintWriter printWriter = new PrintWriter(new FileWriter("output_3.txt", true))) {
            while (isWorking) {
                String linia = input.nextLine();
                if (linia.toLowerCase().equals("quit")) {
                    break;
                }
                printWriter.println(linia);
                printWriter.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
