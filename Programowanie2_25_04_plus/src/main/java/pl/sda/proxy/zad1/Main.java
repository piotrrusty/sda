package pl.sda.proxy.zad1;

public class Main {
    public static void main(String[] args) {
        IRead reader = new MyFileReader();
        System.out.println(reader.readNLines(7));
    }
}
