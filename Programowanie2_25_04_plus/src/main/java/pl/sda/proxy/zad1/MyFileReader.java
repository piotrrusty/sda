package pl.sda.proxy.zad1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MyFileReader implements IRead {
    private BufferedReader reader;

    public void openFile() {
        try {
            reader = new BufferedReader(new FileReader("plik.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public String readLine(){
        if(reader == null){
            openFile();
        }
        String linia = null;
        try {
            linia = reader.readLine();
            if(linia == null){
                przewinPlik();
                linia = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return linia;
    }

    public void pominLinie(){
        readLine();
    }

    public void closeFile(){
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<String> readNLines(int ileLinii){
        List<String> linie = new ArrayList<>();
        for (int i = 0; i<ileLinii; i++){
            linie.add(readLine());
        }
        return linie;
    }

    public void skipNLines(int ileLinii){
        for (int i = 0; i<ileLinii; i++){
            readLine();
        }
    }


    public void przewinPlik(){
        openFile();
    }


}
