package pl.sda.proxy.zad1;

import java.util.List;

public interface IRead {

    public List<String> readNLines(int ileLinii);
    public void skipNLines(int ileLinii);
}
