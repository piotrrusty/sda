package pl.sda.wzorce.zad2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SettingsReader {

    private int zakresLiczby1;
    private int zakresLiczby2;
    private String dzialanie;
    private int iloscRund;

    public int getZakresLiczby1() {
        return zakresLiczby1;
    }

    public void setZakresLiczby1(int zakresLiczby1) {
        this.zakresLiczby1 = zakresLiczby1;
    }

    public int getZakresLiczby2() {
        return zakresLiczby2;
    }

    public void setZakresLiczby2(int zakresLiczby2) {
        this.zakresLiczby2 = zakresLiczby2;
    }

    public String getDzialanie() {
        return dzialanie;
    }

    public void setDzialanie(String dzialanie) {
        this.dzialanie = dzialanie;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }

    public void loadSettings() {
        String switcher = "";
        try (BufferedReader reader = new BufferedReader(new FileReader("settings.txt"))) {
            String line;
            for (int i = 0; i < 3; i++) {
                line = reader.readLine();
                String[] pary = line.split("=");
                int setting = Integer.parseInt(pary[1]);
                switcher = pary[0];
                switch (switcher){
                    case "zakres_liczby_1": MySettings.INSTANCE.setZakresLiczby1(setting);
                    break;
                    case "zakres_liczby_2": MySettings.INSTANCE.setZakresLiczby2(setting);
                    break;
                    case "ilosc_rund": MySettings.INSTANCE.setIloscRund(setting);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showSettings(){
        System.out.println(getZakresLiczby1());
        System.out.println(getZakresLiczby2());
        System.out.println(getIloscRund());
    }
}
