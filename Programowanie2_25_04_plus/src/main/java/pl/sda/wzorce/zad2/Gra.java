package pl.sda.wzorce.zad2;

import java.util.Random;
import java.util.Scanner;

public class Gra {

    boolean isWorking = true;
    int liczba1;
    int liczba2;
    char dzialanie;

    public void gra() {

        Scanner input = new Scanner(System.in);
        Random random = new Random();

        Gracz gracz1 = new Gracz();
        Gracz gracz2 = new Gracz();
        System.out.println("Gracz 1 podaj swoje imie:");
        gracz1.setImie(input.nextLine());
        System.out.println("Gracz 2 podaj swoje imie:");
        gracz2.setImie(input.nextLine());


        while (isWorking) {
            liczba1 = random.nextInt(MySettings.INSTANCE.getZakresLiczby1() + 1);
            liczba2 = random.nextInt(MySettings.INSTANCE.getZakresLiczby2() + 1);

            int licznik = random.nextInt(4);
            double odpowiedz = 0;
            switch (licznik) {
                case 0:
                    dzialanie = '+';
                    System.out.println("Podaj ile to jest: " + liczba1 + dzialanie + liczba2);
                    odpowiedz = input.nextInt();
                    if (odpowiedz == (liczba1 + liczba2)) {
                        gracz1.setIloscPunktow(gracz1.getIloscPunktow() + 1);
                    }
                case 1:
                    dzialanie = '-';
                    System.out.println("Podaj ile to jest: " + liczba1 + dzialanie + liczba2);
                    odpowiedz = input.nextInt();
                    if (odpowiedz == (liczba1 + liczba2)) {
                        gracz1.setIloscPunktow(gracz1.getIloscPunktow() + 1);
                    }
                case 2:
                    dzialanie = '*';
                    System.out.println("Podaj ile to jest: " + liczba1 + dzialanie + liczba2);
                    odpowiedz = input.nextInt();
                    if (odpowiedz == (liczba1 + liczba2)) {
                        gracz1.setIloscPunktow(gracz1.getIloscPunktow() + 1);
                    }
                case 3:
                    dzialanie = '/';
                    System.out.println("Podaj ile to jest: " + liczba1 + dzialanie + liczba2);
                    odpowiedz = input.nextInt();
                    if (odpowiedz == (liczba1 + liczba2)) {
                        gracz1.setIloscPunktow(gracz1.getIloscPunktow() + 1);
                    }
            }


        }


    }

    public int dodaj() {
        int wynik = liczba1 + liczba2;

        return wynik;
    }

    public int odejmij() {
        int wynik = liczba1 - liczba2;

        return wynik;
    }

    public int pomnoz() {
        int wynik = liczba1 * liczba2;

        return wynik;
    }

    public double podziel() {
        double wynik = (double) liczba2 / liczba2;

        return wynik;
    }
}
