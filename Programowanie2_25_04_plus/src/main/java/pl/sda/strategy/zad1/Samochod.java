package pl.sda.strategy.zad1;

public class Samochod {

    private String nrRej;
    private INapęd silnik;
    private double predkosc = 0.0;
    private int poziomAkumulatora = 50;
    private int iloscPaliwa = 60;

    public Samochod(String nrRej, INapęd silnik) {
        this.nrRej = nrRej;
        this.silnik = silnik;
    }

    public String getNrRej() {
        return nrRej;
    }

    public INapęd getSilnik() {
        return silnik;
    }

    public double getPredkosc() {
        return predkosc;
    }

    public int getPoziomAkumulatora() {
        return poziomAkumulatora;
    }

    public int getIloscPaliwa() {
        return iloscPaliwa;
    }

    public void setPredkosc(int predkosc) {
        this.predkosc = predkosc;
    }

    public void setPoziomAkumulatora(int poziomAkumulatora) {
        this.poziomAkumulatora = poziomAkumulatora;
    }

    public void setIloscPaliwa(int iloscPaliwa) {
        this.iloscPaliwa = iloscPaliwa;
    }

    public void przyspiesz(){
        double przyspieszenie = this.getSilnik().getMocSilnika() * 0.1;
        predkosc += przyspieszenie;
        poziomAkumulatora -= this.getSilnik().getPoborPraduSilnika();
        iloscPaliwa -= this.getSilnik().getPoborPaliwaSilnika();

        System.out.println("Przyspieszam o "+ przyspieszenie + " do " +getPredkosc());
    }

    public void zwolnij(){
        if(predkosc>=10){
            predkosc -=10;
        }else{
            predkosc-=predkosc;
        }

        poziomAkumulatora += getSilnik().getPoborPraduSilnika()*0.3;
        System.out.println("Zwalniam o 10km/h do " + getPredkosc());
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nrRej='" + nrRej + '\'' +
                ", silnik=" + silnik +
                ", predkosc=" + predkosc +
                ", poziomAkumulatora=" + poziomAkumulatora +
                ", iloscPaliwa=" + iloscPaliwa +
                '}';
    }
}
