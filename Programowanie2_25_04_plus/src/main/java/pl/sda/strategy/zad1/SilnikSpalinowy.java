package pl.sda.strategy.zad1;

public class SilnikSpalinowy implements INapęd {
    @Override
    public int getPoborPraduSilnika() {
        return 0;
    }

    @Override
    public int getPoborPaliwaSilnika() {
        return 2;
    }

    @Override
    public int getMocSilnika() {
        return 100;
    }
}
