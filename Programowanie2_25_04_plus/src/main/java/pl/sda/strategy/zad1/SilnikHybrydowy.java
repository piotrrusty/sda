package pl.sda.strategy.zad1;

public class SilnikHybrydowy implements INapęd {
    @Override
    public int getPoborPraduSilnika() {
        return 1;
    }

    @Override
    public int getPoborPaliwaSilnika() {
        return 1;
    }

    @Override
    public int getMocSilnika() {
        return 0;
    }
}
