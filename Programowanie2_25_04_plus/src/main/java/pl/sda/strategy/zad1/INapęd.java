package pl.sda.strategy.zad1;

public interface INapęd {

    public int getPoborPraduSilnika();
    public int getPoborPaliwaSilnika();
    public int getMocSilnika();
}
