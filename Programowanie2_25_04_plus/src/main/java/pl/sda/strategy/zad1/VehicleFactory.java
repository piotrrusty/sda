package pl.sda.strategy.zad1;

public abstract class VehicleFactory {

    public static Samochod samochodZSilnikiemHybrydowym(){ return new Samochod("tyhg", new SilnikHybrydowy()); }

    public static Samochod samochodZSilnikiemSpalinowym(){ return new Samochod("reww", new SilnikSpalinowy()); }

    public static Samochod samochodZSilnikiemElektrycznym(){ return new Samochod("cxvb", new SilnikElektryczny()); }

}
