package pl.sda.strategy.zad1;

public class SilnikElektryczny implements INapęd {
    @Override
    public int getPoborPraduSilnika() {
        return 2;
    }

    @Override
    public int getPoborPaliwaSilnika() {
        return 0;
    }

    @Override
    public int getMocSilnika() {
        return 100;
    }
}
