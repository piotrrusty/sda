package pl.sda.strategy.zad0;

public class FormatterInverter implements IFormatterCzcionki {
    @Override
    public String zamienTekst(String tekstDoZmiany) {
        char[] znaki = tekstDoZmiany.toCharArray();

        for (int i = 0; i <znaki.length; i++) {

            if (Character.isUpperCase(znaki[i])) {
                znaki[i] = Character.toLowerCase(znaki[i]);
            } else if (Character.isLowerCase(znaki[i])) {
                znaki[i] = Character.toUpperCase(znaki[i]);
            } else {
                break;
            }
        }
        String wynik = new String(znaki);
        System.out.println(wynik);
        return wynik;
    }
}
