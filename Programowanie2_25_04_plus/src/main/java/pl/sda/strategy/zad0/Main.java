package pl.sda.strategy.zad0;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        Printer printer = new Printer();
        while (true) {
            System.out.println("wpisz komende");
            String komenda = input.nextLine();
            String[] komendy = komenda.split(" ");

            switch (komendy[0]) {
                case "zmien":
                    if (komendy[1].toLowerCase().equals("upper")) {
                        printer.setRodzajWydruku(new FormatterUpper());
                    } else if (komendy[1].toLowerCase().equals("lower")) {
                        printer.setRodzajWydruku(new FormatterLower());
                    } else if (komendy[1].toLowerCase().equals("trim")) {
                        printer.setRodzajWydruku(new FormatterTrim());
                    } else if (komendy[1].toLowerCase().equals("invert")) {
                        printer.setRodzajWydruku(new FormatterInverter());
                    } else if (komendy[1].toLowerCase().equals("split")) {
                        printer.setRodzajWydruku(new FormatterSplitter());
                    } else {
                        break;
                    }
                    break;
                case "formatuj":
                    String tekst = komendy[1];
                    for(int i =2; i<komendy.length; i++){
                        tekst += " " + komendy[i];
                    }
                    tekst = printer.formatuj(tekst);
                    System.out.println(tekst);
            }


        }
    }
}
