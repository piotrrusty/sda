package pl.sda.strategy.zad0;

public class Printer {
    private IFormatterCzcionki rodzajWydruku = new FormatterUpper();

    public Printer() {
    }

    public void setRodzajWydruku(IFormatterCzcionki rodzajWydruku) {
        this.rodzajWydruku = rodzajWydruku;
    }

    public String formatuj(String tekst) {
       String wynik = rodzajWydruku.zamienTekst(tekst);
        return wynik;
    }

    public void print(String tekst){
        System.out.println(tekst);
    }
}
