package pl.sda.strategy.zad0;

public class FormatterSplitter implements IFormatterCzcionki {
    @Override
    public String zamienTekst(String tekstDoZmiany) {
        char[] znaki = tekstDoZmiany.toCharArray();
        String wynik = "";
        for(int i=0; i<znaki.length; i++){
           String roboczy = znaki[i] + " ";
           wynik += roboczy;
        }
        System.out.println(wynik);
        return wynik;
    }
}
