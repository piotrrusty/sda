package pl.sda.abstractFactory.zad1;

public enum COMPUTER_BRAND {
    ASUS, HP, SAMSUNG, APPLE
}
