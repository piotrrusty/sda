package pl.sda.abstractFactory.zad1;

public abstract class AbstractPCFactory {

    public static AbstractPC createABC(){
        return new AsusPC("ABC", COMPUTER_BRAND.ASUS, 60, 0.8, true);
    }

    public static AbstractPC createYHJ(){
        return new AppleMac("YHJ", COMPUTER_BRAND.APPLE, 78, 0.6, true);
    }


}
