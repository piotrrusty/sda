package pl.sda.abstractFactory.zad1;

public class HpPC extends AbstractPC {
    public HpPC(String name, COMPUTER_BRAND marka, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, marka, cpu_power, gpu_power, isOverlocked);
    }

    public static HpPC createULA80(){
        return new HpPC("ULA80", COMPUTER_BRAND.HP, 75, 0.6, false);
    }

    public static HpPC createZYS34(){
        return new HpPC("ZYS34", COMPUTER_BRAND.HP, 60, 0.75, true);
    }
}
