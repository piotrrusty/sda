package pl.sda.abstractFactory.zad1;

public class SamsungPC extends AbstractPC {


    public SamsungPC(String name, COMPUTER_BRAND marka, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, marka, cpu_power, gpu_power, isOverlocked);
    }

    public static SamsungPC createYUO89(){
        return new SamsungPC("YUO89", COMPUTER_BRAND.SAMSUNG, 90, 0.67, true);
    }

    public static SamsungPC createIOP90(){
        return new SamsungPC("IOP90", COMPUTER_BRAND.SAMSUNG, 67, 0.70, false);
    }
}
