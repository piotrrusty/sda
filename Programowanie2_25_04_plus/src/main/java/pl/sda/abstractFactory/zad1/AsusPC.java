package pl.sda.abstractFactory.zad1;

public class AsusPC extends AbstractPC {
    public AsusPC(String name, COMPUTER_BRAND marka, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, COMPUTER_BRAND.ASUS, cpu_power, gpu_power, isOverlocked);
    }

    public static AsusPC createABC(){
        return new AsusPC("ABC", COMPUTER_BRAND.ASUS, 60, 0.8, true);
    }

    public static AsusPC createXYZ50(){
        return  new AsusPC("XYZ50", COMPUTER_BRAND.ASUS, 50, 0.7, true);
    }
}
