package pl.sda.abstractFactory.zad1;

public abstract class AbstractPC {

    protected String name;
    protected COMPUTER_BRAND marka;
    protected int cpu_power;
    protected double gpu_power;
    protected boolean isOverlocked;

    public AbstractPC(String name, COMPUTER_BRAND marka, int cpu_power, double gpu_power, boolean isOverlocked) {
        this.name = name;
        this.marka = marka;
        this.cpu_power = cpu_power;
        this.gpu_power = gpu_power;
        this.isOverlocked = isOverlocked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public COMPUTER_BRAND getMarka() {
        return marka;
    }

    public void setMarka(COMPUTER_BRAND marka) {
        this.marka = marka;
    }

    public int getCpu_power() {
        return cpu_power;
    }

    public void setCpu_power(int cpu_power) {
        this.cpu_power = cpu_power;
    }

    public double getGpu_power() {
        return gpu_power;
    }

    public void setGpu_power(double gpu_power) {
        this.gpu_power = gpu_power;
    }

    public boolean isOverlocked() {
        return isOverlocked;
    }

    public void setOverlocked(boolean overlocked) {
        isOverlocked = overlocked;
    }
}
