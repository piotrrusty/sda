package pl.sda.abstractFactory.zad1;

public class AppleMac extends AbstractPC {
    public AppleMac(String name, COMPUTER_BRAND marka, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, marka, cpu_power, gpu_power, isOverlocked);
    }

    public static AppleMac createQWT9(){
        return new AppleMac("QWT9", COMPUTER_BRAND.APPLE, 100, 0.78, true);
    }

    public static AppleMac createOPI10(){
        return new AppleMac("OPI10", COMPUTER_BRAND.APPLE, 80, 0.75, false);
    }
}
