package pl.sda.abstractFactory.zad2;


public abstract class BikeFactory {

    public static Bike createKROSS5(){
        return new Bike(BIKE_BRAND.KROSS, 1, 5, BIKE_TYPE.BICYCLE);
    }


}
