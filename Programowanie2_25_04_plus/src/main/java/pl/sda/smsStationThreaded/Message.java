package pl.sda.smsStationThreaded;

public class Message {

    String numerDoKogo;
    String tresc;

    public Message(String numerDoKogo, String tresc) {
        this.numerDoKogo = numerDoKogo;
        this.tresc = tresc;
    }

    public String getNumerDoKogo() {
        return numerDoKogo;
    }

    public void setNumerDoKogo(String numerDoKogo) {
        this.numerDoKogo = numerDoKogo;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }
}
