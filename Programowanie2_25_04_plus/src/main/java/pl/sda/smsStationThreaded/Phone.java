package pl.sda.smsStationThreaded;

import pl.sda.observerObservable.zad1.MMSMessage;
import pl.sda.observerObservable.zad1.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {

    private String phoneNumber;
    private List<String> inbox = new ArrayList<>();

    public Phone(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof pl.sda.observerObservable.zad1.Message) {
            pl.sda.observerObservable.zad1.Message wiadomosc = (Message) arg;
            if (wiadomosc.getNumerDoKogo().equals(phoneNumber)) {
                inbox.add(wiadomosc.getTresc());
                System.out.println("Tresc to do mnie " + this);
            } else {
                System.out.println("To nie do mnie " + phoneNumber);
            }
        } else if (arg instanceof MMSMessage) {
            MMSMessage wiadomosc = (MMSMessage) arg;
            if(wiadomosc.getNumerDoKogo().equals(phoneNumber)){
                inbox.add(wiadomosc.getTresc());
                System.out.println("Tresc to do mnie " + this);
            } else {
                System.out.println("To nie do mnie " + phoneNumber);
            }


        }
    }

    @Override
    public String toString() {
        return "Phone{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", inbox=" + inbox +
                '}';
    }
}