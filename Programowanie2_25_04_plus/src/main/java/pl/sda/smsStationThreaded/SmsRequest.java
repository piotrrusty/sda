package pl.sda.smsStationThreaded;

public class SmsRequest implements Runnable{

    private String nrTel;
    private String tresc;

    public SmsRequest(String nrTel, String tresc) {
        this.nrTel = nrTel;
        this.tresc = tresc;
    }

    @Override
    public void run() {

        try {
            Thread.sleep(10*tresc.length());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Wiadomość wysłana");

    }
}
