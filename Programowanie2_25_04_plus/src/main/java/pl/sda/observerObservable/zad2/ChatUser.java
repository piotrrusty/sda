package pl.sda.observerObservable.zad2;

import java.util.List;

public class ChatUser {

    private int id;
    private String nick;
    private List<String> messages;
    private boolean isAdmin;

    public ChatUser(int id, String nick, List<String> messages, boolean isAdmin) {
        this.id = id;
        this.nick = nick;
        this.messages = messages;
        this.isAdmin = isAdmin;
    }

    public ChatUser(int id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
