package pl.sda.observerObservable.zad1;

import java.util.Observable;

public class SmsStation extends Observable {

    public void addPhone(Phone p){
        addObserver(p);
    }

    public void removePhone(Phone p){
        deleteObserver(p);
    }

    public void sendSms(String numerDoKogo, String tresc){
        Message m = new Message(numerDoKogo, tresc);

        setChanged();
        notifyObservers(m);
    }

}
