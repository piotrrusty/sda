package pl.sda.observerObservable.zad1;

public class MMSMessage {

private String numerDoKogo;
private String tresc;
private boolean cosTam;

    public MMSMessage(String numerDoKogo, String tresc, boolean cosTam) {
        this.numerDoKogo = numerDoKogo;
        this.tresc = tresc;
        this.cosTam = cosTam;
    }

    public String getNumerDoKogo() {
        return numerDoKogo;
    }

    public void setNumerDoKogo(String numerDoKogo) {
        this.numerDoKogo = numerDoKogo;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public boolean isCosTam() {
        return cosTam;
    }

    public void setCosTam(boolean cosTam) {
        this.cosTam = cosTam;
    }
}
