<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="pl.sda.tweet.Tweet" %><%--
  Created by IntelliJ IDEA.
  User: na przyklad Piotrek
  Date: 27-06-2018
  Time: 18:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tweets</title>
</head>
<body>

<a href="addtweet.jsp">TWEETNIJ</a><br/><br/>


<%!
    Map<Integer, Tweet> tweety = new HashMap<>();
%>
<%

    String pinnedS = request.getParameter("pinned");
    String title = request.getParameter("title");
    String text = request.getParameter("text");

    boolean pinned;

    if ("yes".equals(pinnedS)) {
        pinned = true;
    } else {
        pinned = false;
    }

    if (title != null) {
        Tweet t = new Tweet(title, text, pinned);
        tweety.put(t.getId(), t);
    }
    if (tweety.isEmpty()) {
        out.print("<br />");
        out.print("Brak wpisów");
        out.print("<br />");
    } else {
        for (Tweet tw : tweety.values()) {
            out.print(tw.getTitle());
            out.print("   |"+ tw.getCreated().toString());
            out.print("<br />");
        }
    }
%>

</body>
</html>
