<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.Duration" %><%--
  Created by IntelliJ IDEA.
  User: na przyklad Piotrek
  Date: 21-06-2018
  Time: 18:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Uptime</title>
</head>
<body>
<%@include file="header.jspf"%>
<br>

<%!
    private static LocalDateTime start = LocalDateTime.now();
%>
<br>
<% LocalDateTime now = LocalDateTime.now();
    Duration between = Duration.between(start, now);
    out.print("UPTIME: "+between.getSeconds()+" seconds");

%>
<br><br>
<%@include file="footer.jspf"%>
</body>
</html>
