<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <title>Hello, Ania!</title>


</head>
<body>
<c:choose>
    <c:when test="${empty param.name}">
Hello, World!
    </c:when>
    <C:otherwise>
        Hello, ${param.name}!
    </C:otherwise>
</c:choose>
Param (name): ${param.name}

</body>
</html>
