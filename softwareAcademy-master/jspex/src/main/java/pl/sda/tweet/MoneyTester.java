package pl.sda.tweet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MoneyTester {

    public static void main(String[] args) {
        Money money1 = new Money(100, false);
        Money money2 = new Money(200, false);
        Money money3 = new Money(300, false);
        Money money = new Money(400, false);

        List<Money> cash = new ArrayList<>();
        cash.add(money);
        cash.add(money1);
        cash.add(money2);
        cash.add(money3);

        System.out.println(cash);

        Collections.sort(cash);

        System.out.println(cash);

    }
}
