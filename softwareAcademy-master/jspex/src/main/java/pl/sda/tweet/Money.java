package pl.sda.tweet;

public class Money implements Comparable<Money> {

    private static int counter = 0;

    private int id;
    private int value;
    private boolean coin;

    public Money(int value, boolean coin) {
        this.id = ++counter;
        this.value = value;
        this.coin = coin;
    }

    @Override
    public String toString() {
        return "Money{" +
                "id=" + id +
                ", value=" + value +
                ", coin=" + coin +
                '}';
    }

    @Override
    public int compareTo(Money other) {
        if (this == other) {
            return 0;
        }
        if (this.value < other.value) {
            return -1;
        } else if (this.value > other.value) {
            return 1;
        }
        return 0;
    }
}