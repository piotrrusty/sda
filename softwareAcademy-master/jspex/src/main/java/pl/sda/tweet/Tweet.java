package pl.sda.tweet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Tweet {

    private static int counter = 0;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private int id;
    private String title;
    private String text;
    private LocalDateTime created;
    private boolean pinned;

    public Tweet(int id){
        this.id = id;
        this.created = LocalDateTime.now();
    }

    public Tweet(String title, String text, boolean pinned){
        this.title = title;
        this.text = text;
        this.pinned = pinned;
        this.created = LocalDateTime.now();
        this.id = ++counter;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public boolean isPinned() {
        return pinned;
    }
}
