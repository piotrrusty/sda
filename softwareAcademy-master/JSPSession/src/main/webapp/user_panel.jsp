<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Panel</title>
</head>
<body>

<%!
    private void renderLoginForm() {
%>
<form action="user_panel.jsp" method="post">

    Podaj login: <input type="text" name="login"><br/>
    <input type="hidden" name="_action" value="login">
    <input type="submit" value="zaloguj">

</form>
<%!
    }
%>
<%!
    private void renderLogoutForm() {
%>
<form action="user_panel.jsp" method="post">
    <input type="hidden" name="_action" value="logout">
    <input type="submit" value="wyloguj">

</form>
<%!
    }
%>


<%
    String action = request.getParameter("_action");
    String userLoginKeyNameInSession = "user_login";

    if ("login".equals(action)) {
        String loginFromRequest = request.getParameter("login");
        if (null != loginFromRequest && !loginFromRequest.isEmpty()) {
            session.setAttribute(userLoginKeyNameInSession, loginFromRequest);
        }
    } else if ("logout".equals(action)) {
        session.removeAttribute(userLoginKeyNameInSession);
    }

    String loginFromSession = (String) session.getAttribute(userLoginKeyNameInSession);

    if (loginFromSession == null) {
        out.print("Witaj, zaloguj się");
        renderLoginForm();

    } else if (loginFromSession != null) {
        out.print("Witaj, " + loginFromSession);
        renderLogoutForm();


    }
%>


</body>
</html>
