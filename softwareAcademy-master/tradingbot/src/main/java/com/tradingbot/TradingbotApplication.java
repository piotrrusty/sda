package com.tradingbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TradingbotApplication {

    public static void main(String[] args) {
        SpringApplication.run(TradingbotApplication.class, args);
    }
}
