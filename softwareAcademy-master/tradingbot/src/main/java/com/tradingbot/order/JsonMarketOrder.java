package com.tradingbot.order;

import org.json.JSONObject;

public class JsonMarketOrder {

    public JSONObject orderJson(){

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("units","1000");
        jsonObject.put("instrument","EUR_USD");
        jsonObject.put("timeInForce","FOK");
        jsonObject.put("type","MARKET");
        jsonObject.put("positionFill","DEFAULT");

        return jsonObject;
    }
}