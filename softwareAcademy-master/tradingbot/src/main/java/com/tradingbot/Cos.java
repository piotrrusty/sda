package com.tradingbot;

import com.tradingbot.domain.AUDUSD;
import com.tradingbot.domain.EURUSD;
import com.tradingbot.repo.AUDUSDRepository;
import com.tradingbot.repo.EURUSDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class Cos {

    @Autowired
    private EURUSDRepository repo1;
    private AUDUSDRepository repo2;

    WebServiceCaller caller = new WebServiceCaller();

    @Scheduled(fixedDelay = 10000)
    public void log() throws IOException {

        EURUSD eurusd = new EURUSD();
        eurusd.setName("EURUSD");
        eurusd.setValue(caller.priceCall().get(0));

        AUDUSD audusd = new AUDUSD();
        audusd.setName("AUDUSD");
        audusd.setValue(caller.priceCall().get(1));

        repo1.save(eurusd);
        repo2.save(audusd);
    }

//    @GetMapping("/getall")
//    public List<EURUSD> getCurrencies(){
//
//        return repo.findAll();
//    }

}
