package com.tradingbot;

import com.google.gson.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.xml.crypto.OctetStreamData;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WebServiceCaller {

    public JSONObject limitOrderCall(String request) {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost httppost = new HttpPost("https://api-fxpractice.oanda.com/v3/accounts/101-004-8211023-001/orders");

        System.out.println("Requesting : " + httppost.getURI());

        try {
            StringEntity entity = new StringEntity(request);
            System.out.println(request);

            httppost.addHeader("Content-Type", "application/json;charset=UTF-8");
            httppost.addHeader("Authorization", "Bearer 4cbcfb1f3fadd955a96d30d6027653b8-41a4ec28f0c1e8a18a97f3a5896f1214");
            httppost.setEntity(entity);

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);

            JSONObject json = new JSONObject(responseBody);
            System.out.println("Response: " + json);

            return json;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject marketOrderCall(String request) {

        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost httppost = new HttpPost("https://api-fxpractice.oanda.com/v3/accounts/101-004-8211023-001/orders");

        System.out.println("Requesting : " + httppost.getURI());

        try {
            StringEntity entity = new StringEntity(request);
            System.out.println(request);

            httppost.addHeader("Content-Type", "application/json;charset=UTF-8");
            httppost.addHeader("Authorization", "Bearer 4cbcfb1f3fadd955a96d30d6027653b8-41a4ec28f0c1e8a18a97f3a5896f1214");
            httppost.setEntity(entity);

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);

            JSONObject json = new JSONObject(responseBody);
            System.out.println("Response: " + json);

            return json;


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public List<BigDecimal> priceCall() throws IOException {
        String url = "https://api-fxpractice.oanda.com/v3/accounts/101-004-8211023-001/pricing?instruments=EUR_USD%2CAUD_USD";

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        request.addHeader("Content-Type", "application/json;charset=UTF-8");
        request.addHeader("Authorization", "Bearer 4cbcfb1f3fadd955a96d30d6027653b8-41a4ec28f0c1e8a18a97f3a5896f1214");
        HttpResponse response = client.execute(request);

        String stringResponse = EntityUtils.toString(response.getEntity());

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(stringResponse);
        JsonObject object = element.getAsJsonObject();
        JsonArray prices = object.getAsJsonArray("prices");
        JsonElement element1 = prices.get(0);

        JsonArray bids = element1.getAsJsonObject().getAsJsonArray("bids");

        JsonElement priceEURUSD = bids.get(0);
        JsonElement priceAUDUSD = bids.get(1);
        BigDecimal currentPriceEURUSD = priceEURUSD.getAsJsonObject().get("price").getAsBigDecimal();
        BigDecimal currentPriceAUDUSD = priceAUDUSD.getAsJsonObject().get("price").getAsBigDecimal();

        List<BigDecimal> pricesArray = new ArrayList<>();
        pricesArray.add(currentPriceEURUSD);
        pricesArray.add(currentPriceAUDUSD);

        return pricesArray;
    }

    public void priceStreamRequest() throws IOException {
        String url = "https://stream-fxpractice.oanda.com/v3/accounts/101-004-8211023-001/pricing/stream?instruments=EUR_USD%2CUSD_CAD";
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        request.addHeader("Authorization", "Bearer 4cbcfb1f3fadd955a96d30d6027653b8-41a4ec28f0c1e8a18a97f3a5896f1214");
        HttpResponse response = client.execute(request);
        String stringResponse = EntityUtils.toString(response.getEntity());
        StreamSource source = new StreamSource();
        source.setSystemId("https://stream-fxpractice.oanda.com/v3/accounts/101-004-8211023-001/pricing/stream?instruments=EUR_USD%2CUSD_CAD");
        OctetStreamData streamData;

    }

}


