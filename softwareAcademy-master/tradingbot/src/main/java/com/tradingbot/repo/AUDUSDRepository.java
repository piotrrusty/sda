package com.tradingbot.repo;

import com.tradingbot.domain.AUDUSD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AUDUSDRepository extends JpaRepository<AUDUSD, Long> {
}
