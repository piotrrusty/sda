package com.tradingbot.repo;

import com.tradingbot.domain.EURUSD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EURUSDRepository extends JpaRepository<EURUSD, Long> {


}
