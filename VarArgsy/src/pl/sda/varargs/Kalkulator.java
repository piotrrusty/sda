package pl.sda.varargs;

public class Kalkulator {
//    int[] liczby;
//    public Kalkulator (int[] liczby){
//        this.liczby = liczby;
//    }


    public int dodaj(int... liczby) {
        int suma = 0;
        for(int liczba:liczby){
            suma += liczba;
        }
        System.out.println(suma);
        return suma;
    }

    public int odejmij(int... liczby){
        int roznica= liczby[0];
        for(int i=1; i<liczby.length; i++){
            roznica -= liczby[i];
        }
        System.out.println(roznica);
        return roznica;
    }
}
