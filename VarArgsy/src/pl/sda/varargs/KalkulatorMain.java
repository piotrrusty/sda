package pl.sda.varargs;

public class KalkulatorMain {
    public static void main(String[] args) {
        String dzialanie = args[0];
        int[] liczby = new int[args.length-1];
        for(int i=0; i<liczby.length; i++){
            try {
            liczby[i] = Integer.parseInt(args[i+1]);

            }catch(NumberFormatException e){
                System.out.println(String.format("Wprowadzona liczba %s, nie jest liczbą", args[i+1]));
            }
        }

        Kalkulator calc1 = new Kalkulator();

        switch (dzialanie){
            case "+":
                System.out.println("Wynik dodawania to "+calc1.dodaj(liczby));
                break;
            case"-":
                System.out.println("Wynik odejmowania to "+calc1.odejmij(liczby));
                break;
        }
    }
}
