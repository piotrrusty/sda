package pl.sda.args;

public class DodawanieParametrami {
    public static void main(String[] args) {
        int[] liczby = zamienNaLiczby(args);
        int suma = obliczSume(liczby);

        System.out.printf("Suma wprowadzonych liczb to %d", suma);
        obliczSume(5,4,8,5,4,8,9,5,4);
    }

    private static int obliczSume(int... liczby) {
        int suma=0;
        for(int liczba: liczby){
            suma += liczba;
        }
        return suma;
    }

    public static int[] zamienNaLiczby(String[] znaki){
        int[] liczby = new int[znaki.length];
        for (int i=0; i<znaki.length; i++){
            liczby[i] = Integer.parseInt(znaki[i]);
        }
        return liczby;
    }
}
