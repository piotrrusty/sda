package pl.sda.pawel;

import java.util.Random;
import java.util.Scanner;

public class ZaDuzoZaMalo {
    public static void main(String[] args) {
        Random random = new Random();
        int tajemnaLiczba = random.nextInt(100)+1;
        Scanner input = new Scanner(System.in);
        int typowanaLiczba;

        do{
            System.out.println("Typuj liczbę");
            typowanaLiczba = input.nextInt();
            if(typowanaLiczba<tajemnaLiczba){
                System.out.println("Podano za małą liczbę!");
            }
            if(typowanaLiczba>tajemnaLiczba){
                System.out.println("Podano za dużą liczbę");
            }
            if(typowanaLiczba==tajemnaLiczba){
                System.out.println("Gratulacje! Odgadnięto liczbę");
            }

        }while(typowanaLiczba!=tajemnaLiczba);

        System.out.println("KONIEC GRY");

    }


}
