package pl.sda.pawel;

import java.util.Scanner;

public class Text {

    static String text;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Wprowadź text");
        text = input.nextLine();

        System.out.println("Czy text zawiera slowo 'ania'?");
        System.out.println(text.contains("ania"));
        System.out.println("Czy text zaczyna się od 'ania'?");
        System.out.println(text.startsWith("ania"));
        System.out.println("Czy text konczy sie slowem ania?");
        System.out.println(text.endsWith("ania"));
        System.out.println("Czy tekst równa się słowu 'ania'?");
        System.out.println(text.equals("ania"));
        System.out.println("Czy text zawiera slowo 'ania' bez wzgl na wielkosc liter?");
        System.out.println(text.toLowerCase().contains("ania"));
        System.out.println("Jaki jest numer indexu slowa 'ania'?");
        System.out.println(text.indexOf("ania"));
    }
}
