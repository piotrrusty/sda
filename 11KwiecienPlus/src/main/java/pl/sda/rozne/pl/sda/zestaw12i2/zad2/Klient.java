package pl.sda.rozne.pl.sda.zestaw12i2.zad2;

import lombok.AllArgsConstructor;

import java.time.LocalDateTime;
import java.time.LocalTime;

@AllArgsConstructor
public class Klient {

    String imie;
    LocalTime czasPrzybycia;
    boolean isPriority;


    @Override
    public String toString() {
        return "Klient{" +
                "imie='" + imie + '\'' +
                ", czasPrzybycia=" + czasPrzybycia +
                ", isPriority=" + isPriority +
                '}';
    }
}
