package pl.sda.rozne.pl.sda.zestaw12i2.zad1;

public class Main {
    public static void main(String[] args) {
        Produkt nic = new Produkt("dsa", 4.5, PodatekProduktu.VAT5);
        Produkt nic2 = new Produkt("dsafds", 45.5, PodatekProduktu.VAT23);
        Produkt nic3 = new Produkt("dsafdssa", 4.58, PodatekProduktu.VAT8);
        Produkt nic4 = new Produkt("dsaddd", 124.5, PodatekProduktu.VAT8);

        Rachunek rachunek = new Rachunek();

        rachunek.getListaZakupionychProduktów().add(nic);
        rachunek.getListaZakupionychProduktów().add(nic2);
        rachunek.getListaZakupionychProduktów().add(nic3);
        rachunek.getListaZakupionychProduktów().add(nic4);

        rachunek.podsumujRachunekNetto();
        rachunek.podsumujRachunekBrutto();
        rachunek.obliczCenyZInnymiStawkamiPodatku();


    }
}
