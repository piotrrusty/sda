package pl.sda.rozne.pl.sda.zestaw12i2.zad1;

import java.util.ArrayList;
import java.util.List;

public class Rachunek {

    List<Produkt> listaZakupionychProduktów = new ArrayList<>();

    public List<Produkt> getListaZakupionychProduktów() {
        return listaZakupionychProduktów;
    }

    public void wypiszRachunek() {
        for (Produkt p : listaZakupionychProduktów) {
            System.out.println(p.getNazwa() + p.getCena());
        }
    }

    public double podsumujRachunekNetto() {
        double wartoscRachunkuNetto = 0;
        for (Produkt p : listaZakupionychProduktów) {
            wartoscRachunkuNetto += p.getCena();
        }
            System.out.println(wartoscRachunkuNetto);
        return wartoscRachunkuNetto;
    }

    public double podsumujRachunekBrutto() {
        double wartoscRachunkuBrutto = 0;
        for (Produkt p : listaZakupionychProduktów) {
            wartoscRachunkuBrutto += p.podajCeneBrutto();
        }
            System.out.println(wartoscRachunkuBrutto);
        return wartoscRachunkuBrutto;
    }

    public double zwrocWartoscPodatku() {
        double roznicaCenNettoBrutto = 0;
        for (Produkt p : listaZakupionychProduktów) {
            roznicaCenNettoBrutto += (p.podajCeneBrutto() - p.getCena());
        }
        return roznicaCenNettoBrutto;
    }

    public void obliczCenyZInnymiStawkamiPodatku() {
        double cenaSumaryczna = 0;
        for (Produkt p : listaZakupionychProduktów) {
            cenaSumaryczna += p.getCena();
        }
        System.out.println("Wartość rachunku bez podatku: "+ cenaSumaryczna + ". Różnica wartości to " + (podsumujRachunekBrutto()-cenaSumaryczna));
        System.out.println("Wartość rachunku z podatkiem 5%: "+ cenaSumaryczna*PodatekProduktu.VAT5.wartosc+". Różnica wartości to " + (podsumujRachunekBrutto()-cenaSumaryczna*PodatekProduktu.VAT5.wartosc));
        System.out.println("Wartość rachunku z podatkiem: ");
        System.out.println("Wartość rachunku z podatkiem: ");


    }
}

