package pl.sda.rozne.pl.sda.zestaw12i2.zad3;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Dyspozytornia {

    Map<String, Zgloszenie> mapaZgloszen = new HashMap<>();

    public static Comparator<Zgloszenie> wedlugCzasu = new Comparator<Zgloszenie>() {
        @Override
        public int compare(Zgloszenie o1, Zgloszenie o2) {
            if (o1.czasZgloszenia.isBefore(o2.czasZgloszenia)) {
                return -1;
            }
            if (o1.czasZgloszenia.isAfter(o2.czasZgloszenia)) {
                return 1;
            }
            return 0;
        }
    };

    void dodajZgloszenie(String tresc, TypZgloszenia typ) {
        String uuid = UUID.randomUUID().toString();
        Zgloszenie zgloszenie = new Zgloszenie(typ, tresc, LocalDateTime.now());
        mapaZgloszen.put(uuid, zgloszenie);
    }

    public List<Zgloszenie> zwrocNajaktualniejszeZgloszenia(int ilosc) {
//        Comparator<Zgloszenie> wedlugCzasu = new Comparator<Zgloszenie>() {
//            @Override
//            public int compare(Zgloszenie o1, Zgloszenie o2) {
//                if (o1.czasZgloszenia.isBefore(o2.czasZgloszenia)) {
//                    return -1;
//                }
//                if (o1.czasZgloszenia.isAfter(o2.czasZgloszenia)) {
//                    return 1;
//                }
//                return 0;
//            }
//        };
        List<Zgloszenie> tymczasowa = new ArrayList<>();
        for (Zgloszenie z: mapaZgloszen.values()) {
            tymczasowa.add(z);
        }
        Collections.sort(tymczasowa, wedlugCzasu);
        List<Zgloszenie> doWyswietlenia = new ArrayList();
        for(int i=0; i<ilosc; i++){
            doWyswietlenia.add(tymczasowa.get(i));
        }
        System.out.println(doWyswietlenia);
        return doWyswietlenia;
    }

    public List<Zgloszenie> zwrocZgloszeniaTypu(TypZgloszenia typ){
        List<Zgloszenie> zgloszeniaTypu = mapaZgloszen.values().stream()
                .filter(Zgloszenie -> (Zgloszenie.getTypZgloszenia()==typ))
                .collect(Collectors.toList());

        zgloszeniaTypu.sort(wedlugCzasu);
        System.out.println(zgloszeniaTypu);
        return zgloszeniaTypu;
    }
}
