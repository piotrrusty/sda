package pl.sda.rozne.pl.sda.zestaw12i2.zad3;

import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        Dyspozytornia dyspozytornia = new Dyspozytornia();

        dyspozytornia.dodajZgloszenie("Napada na bank", TypZgloszenia.POLICJA);
        czekaj();
        dyspozytornia.dodajZgloszenie("Kobita rodzi", TypZgloszenia.KARETKA);
        czekaj();
        dyspozytornia.dodajZgloszenie("Podpalono szkole", TypZgloszenia.STRAŻ);
        czekaj();
        dyspozytornia.dodajZgloszenie("Podpalono sklep", TypZgloszenia.STRAŻ);
        czekaj();
        dyspozytornia.dodajZgloszenie("Kotek na drzewie", TypZgloszenia.ZGŁOSZENIE_INNE);


        dyspozytornia.zwrocNajaktualniejszeZgloszenia(3);
        dyspozytornia.zwrocZgloszeniaTypu(TypZgloszenia.STRAŻ);


    }



    static void czekaj(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
