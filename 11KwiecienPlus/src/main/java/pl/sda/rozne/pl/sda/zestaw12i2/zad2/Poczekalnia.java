package pl.sda.rozne.pl.sda.zestaw12i2.zad2;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Optional;
import java.util.PriorityQueue;

public class Poczekalnia {

    PriorityQueue<Klient> kolejka = new PriorityQueue<>(new Comparator<Klient>() {
        @Override
        public int compare(Klient o1, Klient o2) {
            if(o1.isPriority && !o2.isPriority)
                return -1;
            if(!o1.isPriority && o2.isPriority)
                return 1;
            // at this point emergency == o.emergency
            // since Java 7 you can return Long.compare(termin.getTime(), o.termin.getTime()) instead of the following:
            if(o1.czasPrzybycia.isBefore(o2.czasPrzybycia)) {
                return -1;
            }
            if(o1.czasPrzybycia.isAfter(o2.czasPrzybycia)) {
                return 1;
            }
            return 0;
        }
    });

    public void dodajKlienta(String imie, boolean czyPriorytet){
        LocalTime data = LocalTime.now();
        Klient klient = new Klient(imie, data, czyPriorytet);
        kolejka.add(klient);
    }

    public Optional<Klient> pobierzKlienta(){
        Klient kolejnyKlient = kolejka.poll();

        return Optional.ofNullable(kolejnyKlient);
    }

    public void wypiszKolejnoKlientow(){
        while(!kolejka.isEmpty()){
            System.out.println(pobierzKlienta());
        }
    }
}
