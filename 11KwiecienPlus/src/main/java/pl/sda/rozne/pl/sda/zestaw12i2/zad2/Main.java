package pl.sda.rozne.pl.sda.zestaw12i2.zad2;

public class Main {
    public static void main(String[] args) {
        Poczekalnia poczekalnia = new Poczekalnia();

        poczekalnia.dodajKlienta("Babcia", true);
        opoznij();
        poczekalnia.dodajKlienta("Pan", false);
        opoznij();
        poczekalnia.dodajKlienta("Dziewczynka", false);
        opoznij();
        poczekalnia.dodajKlienta("Ciezarna", true);

//        poczekalnia.pobierzKlienta();

        poczekalnia.wypiszKolejnoKlientow();
    }


    static void opoznij(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
