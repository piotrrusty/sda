package pl.sda.rozne.pl.sda.zestaw12i2.zad1;

public enum PodatekProduktu {

    VAT8(0.08),
    VAT23(0.23),
    VAT5(0.05),
    NO_VAT(0);

    double wartosc;

    PodatekProduktu(double wartosc){
        this.wartosc = wartosc;
    }
}
