package pl.sda.rozne.pl.sda.zestaw12i2.zad4;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
public class Wiadomosc {

    String tresc;
    LocalDateTime dataOdebrania;
}
