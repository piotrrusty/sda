package pl.sda.rozne.pl.sda.zestaw12i2.zad4;

import java.time.LocalDateTime;
import java.util.Stack;

public class SkrzynkaOdbiorcza {

    Stack<Wiadomosc> wiadomosci = new Stack<>();


    void dodajWiadomosc(String tresc){
        Wiadomosc wiad = new Wiadomosc();
        wiad.setTresc(tresc);
        wiad.setDataOdebrania(LocalDateTime.now());
        wiadomosci.push(wiad);
    }

    Wiadomosc usunNajnowszaWiadomosc(){
        Wiadomosc usunieta = wiadomosci.pop();
        System.out.println("Usunięto: " + usunieta);

        return usunieta;
    }
}
