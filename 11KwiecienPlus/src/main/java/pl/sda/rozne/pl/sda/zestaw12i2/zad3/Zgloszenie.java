package pl.sda.rozne.pl.sda.zestaw12i2.zad3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
public class Zgloszenie {

    TypZgloszenia typZgloszenia;
    String trescZgloszenia;
    LocalDateTime czasZgloszenia;

    @Override
    public String toString() {
        return "Zgloszenie{" +
                "typZgloszenia=" + typZgloszenia +
                ", trescZgloszenia='" + trescZgloszenia + '\'' +
                ", czasZgloszenia=" + czasZgloszenia +
                '}';
    }
}
