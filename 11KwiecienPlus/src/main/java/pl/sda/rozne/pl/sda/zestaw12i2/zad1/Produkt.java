package pl.sda.rozne.pl.sda.zestaw12i2.zad1;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Produkt {

    String nazwa;
    double cena;
    PodatekProduktu podatek;

    double podajCeneBrutto(){
        double cenaBrutto = this.cena*(1+podatek.wartosc);

        return cenaBrutto;
    }
}
