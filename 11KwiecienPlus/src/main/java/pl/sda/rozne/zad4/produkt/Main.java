package pl.sda.rozne.zad4.produkt;

public class Main {
    public static void main(String[] args) {

        ProductDatabase database = new ProductDatabase();

        database.dodajProdukt("Pasta Colgate", 4.50, "12345678");
        database.dodajProdukt("Snickers", 1.40, "87654321");
        database.dodajProdukt("Guma Winterfresh", 2.20, "76543218");
        database.dodajProdukt("Piwo Żywiec", 3.10, "12436587");


        CashRegister cashRegister = new CashRegister(database);

        try {
            cashRegister.wyszukajProdukt("12345678");
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
        }

        try {
            database.printExpiry("12345678");
        } catch (ProductExpiredException e) {
            e.printStackTrace();
        }

        Receipt receipt = new Receipt();

    }
}
