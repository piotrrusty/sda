package pl.sda.rozne.zad4.produkt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReceiptEntry {

    Product product;
    int ilosc;

    @Override
    public String toString() {
        return "ReceiptEntry{" +
                "product=" + product +
                ", ilosc=" + ilosc +
                '}';
    }
}
