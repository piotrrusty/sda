package pl.sda.rozne.zad4.produkt;

public class CashRegister {

    private ProductDatabase database;

    public CashRegister(ProductDatabase database){
        this.database = database;
    }

    public Product wyszukajProdukt(String barcode) throws ProductNotFoundException{
        Product szukany = null;
        for (Product p: database.getProductMap().values()) {
            if(p.getBarcode().equals(barcode)){
                szukany=p;
                System.out.println(p);
            }
        }
        if(szukany==null){
            throw new ProductNotFoundException();
        }
        return szukany;
    }
}
