package pl.sda.rozne.zad4.produkt;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Receipt {
    List<ReceiptEntry> wpisy = new ArrayList<>();
    LocalDateTime dataWystawienia;

    double podliczRachunek(){
        double sum=0;
        for (ReceiptEntry r: wpisy) {
           sum += r.getProduct().getPrice();
        }
        return sum;
    }

    int iloscProduktowNaRachunku(){
        int ilosc=0;
        for (ReceiptEntry r: wpisy) {
        ilosc += r.getIlosc();
        }
        return ilosc;
    }

    double obliczPodatek(){
        double podatek=0;
        for(ReceiptEntry r: wpisy){
            podatek=podliczRachunek()*0.23;
        }
        return podatek;
    }

    double obliczBrutto(){
        double brutto = podliczRachunek()+obliczPodatek();
        return brutto;
    }
}
