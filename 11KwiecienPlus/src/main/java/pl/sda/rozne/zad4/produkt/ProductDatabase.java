package pl.sda.rozne.zad4.produkt;


import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class ProductDatabase {

    Map<String, Product> productMap;

    public ProductDatabase() {
        this.productMap = new HashMap<String, Product>();
    }

    public Product wyszukajPoNazwie(String nazwa) {
        Product szukany = null;
        for (Product p : getProductMap().values()) {
            if (p.nazwa.toLowerCase().equals(nazwa.toLowerCase())) {
                szukany = p;
            }
        }
        if (szukany == null) {
            System.out.println("Nie znaleziono produktu");
        }
        return szukany;
    }

    public Product wyszukajPoKodzie(String barcode) {
        Product szukany = null;
        for (Product p : getProductMap().values()) {
            if (p.getBarcode().equals(barcode)) {
                szukany = p;
            }
        }
        if (szukany == null) {
            System.out.println("Nie znaleziono produktu");
        }
        return szukany;
    }

    public void dodajProdukt(String nazwa, double cena, String barcode) {
        if (!getProductMap().keySet().contains(barcode)) {
            Product product = new Product(nazwa, cena, barcode);
            productMap.put(barcode, product);
        } else {
            System.out.println("Produkt już istnieje w bazie");
        }
    }

    public void printExpiry(String barcode) throws ProductExpiredException {
        for (Product p : productMap.values()) {
            if (p.getBarcode().equals(barcode)) {
                LocalDateTime teraz = LocalDateTime.now();
                Duration waznosc = Duration.between(teraz, p.getExpiryDate());
                if (waznosc.isNegative()) {
                    throw new ProductExpiredException();
                } else {
                    System.out.println("Produkt straci wazność za " + waznosc.toDays() + " dni i " + waznosc.minusDays(13).toHours() + " godzin.");
                }

            }

        }
    }

}


