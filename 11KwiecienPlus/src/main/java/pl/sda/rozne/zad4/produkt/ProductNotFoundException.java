package pl.sda.rozne.zad4.produkt;

public class ProductNotFoundException extends Exception {
    public ProductNotFoundException() {
        super("Nie znaleziono produktu");
    }
}
