package pl.sda.rozne.zad4.produkt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class Product {

    String nazwa;
    double price;
    String barcode;
    LocalDateTime expiryDate;

    public Product(String nazwa, double price, String barcode) {
        this.nazwa = nazwa;
        this.price = price;
        this.barcode = barcode;
        this.expiryDate = LocalDateTime.now().plusDays(14);
    }

    @Override
    public String toString() {
        return "Product{" +
                "nazwa='" + nazwa + '\'' +
                ", price=" + price +
                ", barcode='" + barcode + '\'' +
                ", expiryDate=" + expiryDate +
                '}';
    }
}
