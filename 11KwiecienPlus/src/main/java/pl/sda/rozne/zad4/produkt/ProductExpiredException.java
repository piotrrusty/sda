package pl.sda.rozne.zad4.produkt;

public class ProductExpiredException extends Exception {
    public ProductExpiredException() {
        super("Minęła data ważności!");
    }
}
