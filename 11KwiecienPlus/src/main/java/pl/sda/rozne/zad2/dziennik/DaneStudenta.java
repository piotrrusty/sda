package pl.sda.rozne.zad2.dziennik;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DaneStudenta {

    String imie;
    String nazwisko;
    String nrIndexu;
}
