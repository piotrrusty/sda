package pl.sda.regexp.cwiczenia;

public class WyrazeniaRegularne {
    public static String zlacz(String a, String b){
        a = a.trim().substring(2);
        b=b.trim().substring(0, b.trim().length()-2);
        return a.concat(b);
    }


    public static void main(String[] args) {
        System.out.println(zlacz("    ala ma kota  ", "     raz dwa trzy  "));
        System.out.println(usunSamogloski("Ala ma kota"));
        System.out.println(usunLiczby("Ala ma 20 kotów i 2 psy. Oraz 345 rybek."));
        System.out.println(zamianaZDuzejLitery("Ala ma Ala ma kota", "Bartek"));
        System.out.println(kropkaINowaLinia("Ala ma kota. Kot ma wpierdol."));
    }

    public static String usunSamogloski (String a){
        return a.replaceAll("a|e|i|o|u|y","");

    }

    public static String usunLiczby (String a) {
        return a.replaceAll("\\d{2,}", "");
    }

    public static String zamianaZDuzejLitery (String a, String b){
        return a.replaceAll("[A-Z]{1}[a-z]{1,}",b);
    }

    public static String kropkaINowaLinia (String a) {
        return a.replaceAll("\\.", ".\n");
    }
}
