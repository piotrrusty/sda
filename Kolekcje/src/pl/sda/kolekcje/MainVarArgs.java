package pl.sda.kolekcje;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainVarArgs {
    public static void main(String[] args) {

    }

    public static <T> List<T> dodajDoListy(T... elementy) {
        List<T> lista = new ArrayList<>();
        for (T element : elementy) {
            lista.add(element);
        }
        return lista;
    }

    public static <T> Set<T> dodajDoZbioru(T... elementy) {
        Set<T> zbior = new HashSet<>();
        for (T element : elementy) {
            zbior.add(element);

        }
        return zbior;
    }
}
