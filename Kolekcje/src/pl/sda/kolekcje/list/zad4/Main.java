package pl.sda.kolekcje.list.zad4;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        int liczba = 0;
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            liczba = random.nextInt(100) + 1;
            integers.add(liczba);

        }
        System.out.println(integers);
        int suma = 0;
        for (Integer i : integers) {
            suma += i;
        }
        System.out.println("suma to: " + suma);

        int srednia = suma / integers.size();

        System.out.println("średnia to: " + srednia);

        ArrayList<Integer> doUsuniecia = new ArrayList<>();
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) > srednia) {
                doUsuniecia.add(integers.get(i));
            }
        }
        System.out.println(integers);
        System.out.println("DO USUNIĘCIA: " + doUsuniecia);

        for (int i = 0; i < doUsuniecia.size(); i++) {
            integers.remove(doUsuniecia.get(i));
        }
        System.out.println("PO USUNIĘCIU :" + integers);

        ArrayList<Integer> kopia = new ArrayList<>(integers);
        System.out.println(kopia);
    }
}
