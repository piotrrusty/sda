package pl.sda.kolekcje.list.zad3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String> stringis = Arrays.asList("10030", "3004", "4000", "12355", "12222", "67888", "111200", "225355", "2222", "1111", "3546", "138751", "235912");
        ArrayList<String> stringi = new ArrayList<>(stringis);
        System.out.println(stringi.indexOf("138751"));
        System.out.println(stringi.contains("67888"));
        System.out.println(stringi.contains("67889"));

        stringi.remove("67888");
        stringi.remove("67889");

        System.out.println(stringi.contains("67888"));
        System.out.println(stringi.contains("67889"));

        for (String s: stringi) {
            System.out.print(s + " ");
        }

        for (String s: stringi) {
            System.out.println(s);
        }


    }
}
