package pl.sda.kolekcje.list.zad2;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            int r = random.nextInt(1000);
            integers.add(r);
        }

        int suma = 0;

//        for(int i=0; i<integers.size(); i++){
//            suma += integers.get(i);
//        }
//        System.out.println(suma);

        for (Integer i : integers) {
            suma += i;
        }
        System.out.println(suma);

        double srednia = (double) suma / integers.size();

        System.out.println(srednia);
        System.out.println(integers);

        //Collections.sort(integers);
        System.out.println(integers);

        double mediana = (integers.get(4) + integers.get(5)) / 2;

        System.out.println("Mediana to: " + mediana);

        int najwieksza = 0;
        int najmniejsza = integers.get(0);
        int indexNajw = 0;
        int indexNajm = 0;
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) > najwieksza) {
                najwieksza = integers.get(i);
                indexNajw = i;
            }
           // najmniejsza = integers.get(0);
            if (integers.get(i) < najmniejsza) {
                najmniejsza = integers.get(i);
                indexNajm = i;
            }
        }
        System.out.println("Najwieksza liczba to " + najwieksza + "jej index to: " + indexNajw);
        System.out.println("Najmniejsza liczba to " + najmniejsza + "jej index to: " + indexNajm);


    }
}
