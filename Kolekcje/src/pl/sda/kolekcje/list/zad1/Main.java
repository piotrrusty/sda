package pl.sda.kolekcje.list.zad1;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        Random random = new Random();

        for(int i=0; i<5;i++){
            System.out.println("Podaj liczbę" + (i+1));
            int cos = input.nextInt();
            integers.add(cos);
        }

        for(int i=0; i<5; i++){
            int cos = random.nextInt(100);
            integers.add(cos);
        }

        System.out.println(integers);

        for (Integer i: integers) {
            System.out.println(i);
        }
    }
}
