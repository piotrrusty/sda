package pl.sda.kolekcje.list.zad5;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        Student student1 = new Student(List.of(4.5, 4.6, 3.3, 4.6, 5.0), 6573634, "Wacław", "Kiepski");
        Student student2 = new Student(List.of(4.2, 3.3, 5.1, 2.9, 3.9), 5643765, "Ilona", "Kowalska");
        Student student3 = new Student(List.of(4.3, 2.9, 3.9, 4.4, 3.6), 6473624, "Robert", "Maliniak");
        Student student4 = new Student(List.of(3.3, 2.3, 5.1, 4.3, 4.4), 7363532, "Zdzichu", "Wichura");

        Dziennik dziennik = new Dziennik();

        dziennik.dodajStudenta(student1);
        dziennik.dodajStudenta(student2);
        dziennik.dodajStudenta(student3);
        dziennik.dodajStudenta(student4);

        dziennik.podajSredniaStudenta(6573634);
    }
}
