package pl.sda.kolekcje.list.zad5;

import java.util.ArrayList;
import java.util.List;

public class Dziennik {

    List<Student> studenci = new ArrayList<>();


    public void dodajStudenta(Student student) {
        studenci.add(student);
    }

    public void usunStudenta(Student student) {
        studenci.remove(student);
    }

    public void usunStudenta(int numerNaLiscie) {
        studenci.remove(numerNaLiscie);
    }

    public Student zwrocStudenta(int numerNaLiscie) {
        return studenci.get(numerNaLiscie);
    }

    public double podajSredniaStudenta(int indeksStudenta) {
        for (Student student : studenci) {
            if (student.nrIndeksu == indeksStudenta){
                double sumaOcen=0;
                double srednia=0;
                for (int i = 0; i< student.oceny.size(); i++) {
                    sumaOcen += student.oceny.get(i);
                }
                srednia = sumaOcen/ student.oceny.size();
                System.out.println(srednia);
                return srednia;
            }
        }
        return 0.0;
    }

    public List<Student> podajStudentowZagrozonych() {
        List<Student> zagrozeni = new ArrayList<>();
        for (Student s: studenci) {
            for(int i = 0; i<s.oceny.size(); i++){
                if(s.oceny.get(i)<2.5){
                    zagrozeni.add(s);
                }
            }
        }return zagrozeni;
    }

}
