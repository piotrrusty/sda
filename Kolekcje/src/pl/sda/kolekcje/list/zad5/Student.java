package pl.sda.kolekcje.list.zad5;

import java.util.ArrayList;
import java.util.List;

public class Student {

    List<Double> oceny;
    long nrIndeksu;
    String imie;
    String nazwisko;

    public Student(List<Double> oceny, long nrIndeksu, String imie, String nazwisko) {
        this.oceny = oceny;
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public long getNrIndeksu() {
        return nrIndeksu;
    }
}
