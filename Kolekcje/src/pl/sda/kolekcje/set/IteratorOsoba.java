package pl.sda.kolekcje.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class IteratorOsoba {

    public static void main(String[] args) {

        Osoba osoba1 = new Osoba("Roman", "Zając", 60);
        Osoba osoba2 = new Osoba("Anna", "Roman", 16);
        Osoba osoba3 = new Osoba("Robert", "Maliniak", 34);

        Set<Osoba> ludzie = new HashSet<>();

        ludzie.add(osoba1);
        ludzie.add(osoba2);
        ludzie.add(osoba3);

//        Iterator<Osoba> iterator = ludzie.iterator();

//        while(iterator.hasNext()){
//            Osoba osoba = iterator.next();
//            if (osoba.wiek<18){
//                iterator.remove();
//            }
//            System.out.println(osoba.toString());
//        }
        Osoba[] osoby = new Osoba[ludzie.size()];

        ludzie.toArray(osoby);
        for(Osoba osoba : osoby){
            if(osoba.wiek>18){
                ludzie.remove(osoba);
            }
        }

        System.out.println(ludzie);
    }
}

class Osoba {
    String imie;
    String nazwisko;
    int wiek;

    public Osoba(String imie, String nazwisko, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wiek=" + wiek +
                '}';
    }
}
