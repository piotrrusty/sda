package pl.sda.kolekcje.set;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Set<String> set = new HashSet<>();
        Scanner input = new Scanner(System.in);

        set.add(input.nextLine());
        set.add(input.nextLine());
        set.add(input.nextLine());
        set.add(input.nextLine());
        set.add(input.nextLine());

        System.out.println(set);

    }
}
