package pl.sda.kolekcje.set;

import java.util.*;

public class ZadanieLekcja {
//    Wykonaj polecenia:
//    a. Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:
//    b. Wypisz liczbę elementów na ekran (metoda size())
//    c. Wypisz wszystkie zbioru elementy na ekran (forEach)
//    d. Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
//
//    e. Napisz metodę sprawdzającą, czy w tekście nie powtarzają się litery z wykorzystaniem zbioru. (boolean containDuplicates(String text))
//    f. Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
//    {(1,2), (2,1), (1,1), (1,2)}.
//    g. Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?

    public static void main(String[] args) {
        Integer[] inty = new Integer[]{10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55};
        Set<Integer> integers = new HashSet<>(Arrays.asList(inty));

        System.out.println(integers.size());

        for (Integer i:integers) {
            System.out.println(i);
        }
        integers.remove(10);
        integers.remove(12);

        Integer[] inty2 = new Integer[]{3, 4, 300, 40, 55};
        Set<Integer> integers2 = new HashSet<>(Arrays.asList(inty2));
        System.out.println(integers2.size());
        containDuplicates("Alobijuekw");

    }

    public static boolean containDuplicates (String tekst){
        String[] tab = tekst.split("");
        Set<String> zdanie = new HashSet<>(Arrays.asList(tab));
        System.out.println(tekst);
        if(tab.length!=zdanie.size()){
            System.out.println("Usunięto duplikaty");
            return true;
        }
        System.out.println("Duplikatów nie znaleziono");
        System.out.println(zdanie);
        return false;
    }
}
