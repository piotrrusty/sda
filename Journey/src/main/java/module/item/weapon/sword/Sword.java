package module.item.weapon.sword;

public enum Sword {
    DAGGER(5.4, 1.4),
    CLUB(6.7, 3.0),
    SHORT_SWORD(10.0, 4.5);

    private double atk;
    private double weight;

    Sword(double atk, double weight) {
    }

    public double getAtk() {
        return atk;
    }

    public void setAtk(double atk) {
        this.atk = atk;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
