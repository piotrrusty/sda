package module.character;

import lombok.Getter;
import lombok.Setter;
import module.item.Item;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Player {
    String name;
    List<Item> inventory = new ArrayList<Item>();
    Menu menu = new Menu();


    public void show(List zawartosc) {
        for (Object i: zawartosc) {
            System.out.println(i);
        }
    }

}
