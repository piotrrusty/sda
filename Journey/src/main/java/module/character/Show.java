package module.character;

import java.util.List;

public interface Show<L> {

    void show(List<L> zawartosc);

}
