import java.util.Scanner;

public class Main {
    private boolean gameOn = true;
    private static Menu mainMenu;


    public static void main(String[] args) {
        Game game = new Game();

        String wybor;
        do {
            Scanner input = new Scanner(System.in);
            System.out.println("Cześć, co chcesz zrobić?");
            wybor = input.nextLine();
            if (wybor.toLowerCase().contains("n")) {
                game.graj();
            }
            if (wybor.toLowerCase().contains("l")) {
                mainMenu.loadGame();
            }
            if (wybor.toLowerCase().contains("o")) {
                mainMenu.options();
            }


        } while (!wybor.toLowerCase().contains("n"));
    }

}
