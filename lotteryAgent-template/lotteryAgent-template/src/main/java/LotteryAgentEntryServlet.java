import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by krzysztof on 12.11.17.
 */

@WebServlet("/play")
public class LotteryAgentEntryServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LotteryAgentEntryServlet.class);

    private List<Integer> createRandomNumbers() {

        Set<Integer> numbers = new HashSet<>();
        while (numbers.size()<6) {
            numbers.add(new Random().nextInt(49+1));
        }

        return new ArrayList<>(numbers);
    }

    /**
     * Wypisanie odpowiedzi za pomocą getWriter()
     *
     * @param resp
     * @param randomNumbers
     */
    private void writeResponseToClient(HttpServletResponse resp, List<Integer> randomNumbers) throws IOException {

        Logger.getLogger(LotteryAgentEntryServlet.class).info("Informacja: " + randomNumbers);
        logger.info("Informacja: " +randomNumbers);
        PrintWriter writer = resp.getWriter();
        writer.println("Twoje liczby to: " + randomNumbers);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doGet(req, resp);

        writeResponseToClient(resp, createRandomNumbers());
    }
}
