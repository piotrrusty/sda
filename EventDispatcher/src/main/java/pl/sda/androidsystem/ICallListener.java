package pl.sda.androidsystem;

import java.time.LocalDateTime;

public interface ICallListener {

    public void callStarted(int call_id);

    public void callEnded(int call_id);
}
