package pl.sda.androidsystem.events;

import pl.sda.androidsystem.IEvent;

import java.time.LocalDateTime;

public class CallStartedEvent implements IEvent {

    private int callID;
    private LocalDateTime startTime;

    public LocalDateTime getCallStartTime() {
        return startTime;
    }

    public void setCallStartTime(LocalDateTime callStartTime) {
        this.startTime = callStartTime;
    }

    public int getCallID() {
        return callID;
    }

    public void setCallID(int callID) {
        this.callID = callID;
    }

    @Override
    public void execute() {


    }
}
