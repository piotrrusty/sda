package pl.sda.androidsystem.events;

import java.time.LocalDateTime;

public class CallEndedEvent {

    private int callID;
    private LocalDateTime endedTime;

    public int getCallID() {
        return callID;
    }

    public void setCallID(int callID) {
        this.callID = callID;
    }

    public LocalDateTime getEndedTime() {
        return endedTime;
    }

    public void setEndedTime(LocalDateTime endedTime) {
        this.endedTime = endedTime;
    }
}
