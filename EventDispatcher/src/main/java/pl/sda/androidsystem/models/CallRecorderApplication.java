package pl.sda.androidsystem.models;

import pl.sda.androidsystem.EventDispatcher;
import pl.sda.androidsystem.ICallListener;
import pl.sda.androidsystem.events.CallStartedEvent;

import java.util.ArrayList;
import java.util.List;

public class CallRecorderApplication implements ICallListener {

    List<CallStartedEvent> startEvents = new ArrayList<>();

    public CallRecorderApplication() {
        EventDispatcher.getInstance().register(this);
    }

    @Override
    public void callStarted(int call_id) {


    }

    @Override
    public void callEnded(int call_id) {

    }
}
