package pl.sda.androidsystem.models;

import pl.sda.androidsystem.EventDispatcher;
import pl.sda.androidsystem.ICallListener;

import java.time.LocalDateTime;
import java.util.Date;

public class PhoneApplication implements ICallListener {

    private int callID = 0;
    private LocalDateTime timeStarted;
    private LocalDateTime timeEnded;

    public PhoneApplication() {
        EventDispatcher.getInstance().register(this);
    }

    @Override
    public void callStarted(int call_id) {
        timeStarted = LocalDateTime.now();
        callID++;
    }

    @Override
    public void callEnded(int call_id) {
        timeEnded = LocalDateTime.now();
    }
}
