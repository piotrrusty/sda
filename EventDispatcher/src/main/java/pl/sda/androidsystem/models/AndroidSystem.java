package pl.sda.androidsystem.models;

import pl.sda.androidsystem.EventDispatcher;
import pl.sda.androidsystem.ICallListener;

public class AndroidSystem implements ICallListener {

    public AndroidSystem() {
        EventDispatcher.getInstance().register(this);
    }

    @Override
    public void callStarted(int call_id) {

    }

    @Override
    public void callEnded(int call_id) {

    }
}
