package zad1;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class RegistrationDesk {

    Map<String, Patient> patientMap = new HashMap<>();

    public RegistrationDesk() {

    }

    public Map<String, Patient> getPatientMap() {
        return patientMap;
    }

    void addPatient(Patient patient) {
        Patient patient1 = getPatient(patient.getPesel());
        if(patient1==null){
            patientMap.put(patient.getPesel(), patient);
        }
    }


    Patient getPatient(String pesel) {

        return patientMap.get(pesel);
    }

    void savePatients() {

        File file = new File("patients.txt");


        try {
            PrintWriter printWriter = new PrintWriter("patients.txt");
            for (Patient patient : patientMap.values()) {
                printWriter.print(patient.getName() + " ");
                printWriter.print(patient.getSurname() + " ");
                printWriter.print(patient.getPesel());
                printWriter.println();
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    void loadPatients() {

        File plik = new File("patients.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(plik))) {

            String linia = reader.readLine();
            while (linia != null) {
                String[] tab = linia.split(" ");
                Patient toAdd = new Patient(tab[0], tab[1], tab[2]);

                patientMap.put(tab[2], toAdd);
                linia = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    void printPatients() {
        for (Patient patient : patientMap.values()) {
            System.out.println(patient);
        }
    }
}

