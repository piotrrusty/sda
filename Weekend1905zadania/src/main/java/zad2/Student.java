package zad2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Student {

    String name;
    String surname;
    String indexNumber;
    int year;
    Map<String, List<Double>> mapaOcen;
    Map<String, Double> listaSrednich;

    public Student(String name, String surname, String indexNumber, int year, double gradesMean) {
        this.name = name;
        this.surname = surname;
        this.indexNumber = indexNumber;
        this.year = year;
        this.mapaOcen = new HashMap<>();

        for (Subject subject : Subject.values()) {
            mapaOcen.put(subject.toString(), new ArrayList<Double>());
        }
    }

    public Student(String s, String s1, String s2, int i) {
    }

    public double obliczSredniaOcenZPrzemiotu(String przedmiot) {

        double srednia = 0;
        if (mapaOcen.containsKey(przedmiot)) {
            List<Double> listaOcen = mapaOcen.get(przedmiot);

            double suma = 0;
            for (Double ocena : listaOcen) {
                suma += ocena;
            }
            srednia = suma / listaOcen.size();
        }
        return srednia;
    }

    public double obliczSredniaOcenZeWszystkichPrzedmiotow(){

        double suma=0;
        for (String przedmiot: mapaOcen.keySet()) {
            suma += obliczSredniaOcenZPrzemiotu(przedmiot);
        }
        double srednia = suma/mapaOcen.keySet().size();

        return srednia;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Map<String, List<Double>> getMapaOcen() {
        return mapaOcen;
    }

    public Map<String, Double> getListaSrednich() {
        return listaSrednich;
    }

}
