package zad2;

import java.io.*;
import java.util.*;

public class Dziekanat {

    private Map<String, Student> mapaStudentow;

    public Dziekanat() {
        this.mapaStudentow = new HashMap<>();
    }

    void dodajStudenta(Student student) {

        if (!mapaStudentow.containsKey(student.getIndexNumber())) {
            mapaStudentow.put(student.getIndexNumber(), student);
        } else {
            System.out.println("Student już w rejestrze!");
        }
    }

    public Optional<Student> wybierzStudenta(String nrIndexu) {

//        Student szukany = null;
//        if (!mapaStudentow.containsKey(nrIndexu)) {
//            System.out.println("Student o podanym indexie nie istnieje");
//        } else {
//            szukany = mapaStudentow.get(nrIndexu);
//
//        }
//        return szukany;
        return Optional.ofNullable(mapaStudentow.get(nrIndexu));
    }

    public Optional<Student> wybierz(String nrIndexu) {
//        if (!mapaStudentow.containsKey(nrIndexu)) {
//            return Optional.empty();
//        }
        return Optional.ofNullable(mapaStudentow.get(nrIndexu));
    }


    public List<Student> wybierzStudentowZeSredniaNizszaNiz(double sredniaProgowa) {

        List<Student> lista = new ArrayList<>();
        for (Student s : mapaStudentow.values()) {
            double srednia = s.obliczSredniaOcenZeWszystkichPrzedmiotow();
            if (srednia < sredniaProgowa) {
                lista.add(s);
            }

        }
        return lista;
    }

    public List<Student> wybierzStudentowZRoku(int rok) {
        List<Student> studenciZRoku = new ArrayList<>();
        for (Student s : mapaStudentow.values()) {
            if (s.getYear() == rok) {
                studenciZRoku.add(s);
            }
        }
        return studenciZRoku;
    }

    public boolean usunStudenta(String nrIndexu) {

        if (mapaStudentow.containsKey(nrIndexu)) {
            mapaStudentow.remove(nrIndexu);
            return true;
        } else {
            System.out.println("Podanego studenta nie ma bazie");
            return false;
        }
    }

    public void zapiszWPliku() {
        File file = new File("studenci.txt");

        try {
            PrintWriter printWriter = new PrintWriter("studenci.txt");
            for (Student s : mapaStudentow.values()) {
                printWriter.print(s.getIndexNumber() + " ");
                printWriter.print(s.getName() + " ");
                printWriter.print(s.getSurname() + " ");
                printWriter.print(s.obliczSredniaOcenZeWszystkichPrzedmiotow());
                printWriter.println();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void odczytZPliku() {
        File file = new File("studenci.txt");


        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String linia = reader.readLine();
            while(linia != null) {
                String[] tab = linia.split(" ");
                Student student = new Student(tab[0], tab[1], tab[2], Integer.parseInt(tab[3]));

                mapaStudentow.put(student.getIndexNumber(), student);
                linia = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Map<String, Student> getMapaStudentow() {
        return mapaStudentow;
    }
}