package zad2.options;

import zad2.Dziekanat;
import zad2.Student;

import java.util.Optional;
import java.util.Scanner;

public class OptionSearchByIndex implements IOption {
    @Override
    public String printOption() {
        return "Wyszukaj po indeksie";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please input index number: ");
        String index = input.nextLine();

        Optional<Student> student = dziekanat.wybierzStudenta(index);
        System.out.println(student);

    }
}
