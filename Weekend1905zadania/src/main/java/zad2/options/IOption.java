package zad2.options;

import zad2.Dziekanat;

public interface IOption {

    public String printOption();
    public void enterOption(Dziekanat dziekanat);
}
