package zad2.options;

import zad2.Dziekanat;
import zad2.Student;

import java.util.List;
import java.util.Scanner;

public class OptionSearchByGrades implements IOption {
    @Override
    public String printOption() {
        return "Wybierz ze średnia niższa niż";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        Scanner input = new Scanner(System.in);

        System.out.println("Gimme grades: ");
        try{
            Double gradesMean = Double.parseDouble(input.nextLine());

            List<Student> studentList = dziekanat.wybierzStudentowZeSredniaNizszaNiz(gradesMean);
            studentList.forEach(System.out::println);
        }catch (NumberFormatException nfe) {
            System.err.println("Error, wrong input!");
            return;
        }

    }
}
