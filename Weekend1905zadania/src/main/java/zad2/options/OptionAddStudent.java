package zad2.options;

import zad2.Dziekanat;
import zad2.Student;

import java.util.Scanner;

public class OptionAddStudent implements IOption {
    @Override
    public String printOption() {
        return "Dodaj Studenta";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        Scanner input = new Scanner(System.in);
        System.out.println("Index:");
        String index = input.nextLine();
        System.out.println("Name:");
        String name = input.nextLine();
        System.out.println("Surname");
        String surname = input.nextLine();

        try{
            System.out.println("Year:");
            String year = input.nextLine();
            int rok = Integer.parseInt(year);

            System.out.println("Grades:");
            String grades = input.nextLine();
            double gradesMean = Double.parseDouble(grades);

            Student student = new Student(name, surname, index, rok, gradesMean);

        }catch (NumberFormatException nfe) {
            System.err.println("Wrong input!");
        }

    }
}
