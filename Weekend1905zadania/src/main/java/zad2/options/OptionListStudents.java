package zad2.options;

import zad2.Dziekanat;

public class OptionListStudents implements IOption {
    @Override
    public String printOption() {
        return "Wylistuj studentów";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        dziekanat.getMapaStudentow().values().forEach(System.out::println);

    }
}
