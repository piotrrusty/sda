package com.sda.annotations.zad1;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
    @Target(value = ElementType.FIELD)
    @Retention(value = RetentionPolicy.RUNTIME)

public @interface MyAnnotation {


    String value();
    int count();
}
