package com.sda.annotations.zad2;

public class Osoba {

    public Osoba(String imie, int wiek) {
        setImie(imie);
        this.wiek = wiek;
    }

    @MaxLength(maxLength = 100)
    String imie;
    int wiek;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
        if(!LengthVerification.czyMiesciSieWLimicie(this)){
            throw new IllegalArgumentException("Limit długości przekroczony");
        }
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", wiek=" + wiek +
                '}';
    }
}
