package com.sda.annotations.zad2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
public @interface MaxLength {
    int maxLength() default 25;
}
