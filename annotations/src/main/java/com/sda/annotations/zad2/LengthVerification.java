package com.sda.annotations.zad2;

import java.lang.reflect.Field;

public class LengthVerification {

    public boolean LengthVerificationPassed;

    public static boolean czyMiesciSieWLimicie(Object obiekt){

        Field[] pola = obiekt.getClass().getDeclaredFields();

        for (Field pole: pola) {
            MaxLength adnotacja = pole.getAnnotation(MaxLength.class);
            if(adnotacja!=null){
                int max = adnotacja.maxLength();

                try{
                    pole.setAccessible(true);
                    String wartoscPola = (String) pole.get(obiekt);
                    if(wartoscPola.length()>max){
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }
     return true;
    }


}
