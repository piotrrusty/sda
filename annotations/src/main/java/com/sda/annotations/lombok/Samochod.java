package com.sda.annotations.lombok;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Samochod {
    private String nazwa;
    private int iloscMiejsc;
    private String marka;
}
