package com.sda.grawkarty;


public class Karta {

    private String kolor;
    private String wartosc;

    /**
     * Konstruktor klasy karta, ktory wymaga podania obu argumentów
     * @param kolor - tj. pik, serce, karo , trefl
     * @param wartosc - wartosc 2,3,4,5,6,7,8,9,10, J, D, K, A
     */
    public Karta(String kolor, String wartosc) {
        this.kolor = kolor;
        this.wartosc = wartosc;
    }

    /**
     * Metoda pobiera wartośc koloru z pola
     * @return - zrócona wartość pola
     */
    public String getKolor() {
        return kolor;
    }

    /**
     * Metoda ustawia wartośc koloru
     * @param kolor - wartość do ustawienia
     */
    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    /**
     * Metoda pobiera wartość figury z pola
     * @return - zwrocona wartość z pola
     */
    public String getWartosc() {
        return wartosc;
    }

    /**
     * Metoda ustawia wartośc figury
     * @param wartosc - wartość do ustawienia
     */
    public void setWartosc(String wartosc) {
        this.wartosc = wartosc;
    }
}
