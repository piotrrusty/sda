public class TypyDanych {
    public static void main(String[] args) {
        System.out.println("tekst :"+'a'+'b');
        System.out.println("a"+"b");
        System.out.println('A'+2);
        System.out.println(100L-10);
        System.out.println('a' + 'b' + 3);
        System.out.println(9%4);
        System.out.println(5/2.0);
        System.out.println(6754%8);
        System.out.println('b'+3+'a');

        int a = 1;
        int b = a++;

        System.out.println(b);
        System.out.println(a);

        boolean test = 2>3;
        boolean test2 = !test;
        boolean test3 = (2<3) && (4>2);

        System.out.println(test);
        System.out.println(test2);
        System.out.println("test3="+test3);

        boolean test4 = false==false;
        System.out.println(test4);
        boolean test5 = false != false;
        System.out.println(test5);
        boolean test6 = !true;
        System.out.println(test6);
        boolean test7 = 2>4;
        System.out.println(test7);
        boolean test8 = 3==3 && 3==4;
        System.out.println(test8);
        boolean test9 = 3!=5 && 3==5;
        System.out.println(test9);
        boolean test10 = (2+4)>(1+3);
        System.out.println(test10);
        boolean test11 = "cos".equals("cos");
        System.out.println(test11);
        boolean test12 = "cos" == "cos";
        System.out.println(test12);

        short tysiac = 1000;
        byte tysiacByte = (byte)tysiac;
        System.out.println(tysiacByte);

        short sho = 5;
        int shoInt = sho;
        System.out.println(sho);
        long shoLong = sho;
        System.out.println(sho);
        int integer = 345463;
        float floating = integer;
        System.out.println(integer);
        double intDouble = integer;
        System.out.println(integer);
        long longina = 3746581;
        int longInt = (int)longina;
        System.out.println(longina);
        byte bajcik = (byte)sho;
        System.out.println(bajcik);
        char czar = 'd';
        int last = czar;
        System.out.println(last);
    }
}
