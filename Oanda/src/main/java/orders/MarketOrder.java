package orders;

import com.oanda.v20.order.OrderType;
import com.oanda.v20.order.TimeInForce;
import com.oanda.v20.primitives.DecimalNumber;
import com.oanda.v20.primitives.InstrumentName;
import com.oanda.v20.transaction.StopLossDetails;
import com.oanda.v20.transaction.TakeProfitDetails;
import com.oanda.v20.transaction.TrailingStopLossDetails;

public class MarketOrder {

    OrderType type;
    InstrumentName name;
    DecimalNumber units;
    TimeInForce timeInForce;
    TakeProfitDetails takeProfitDetails;
    StopLossDetails stopLossDetails;
    TrailingStopLossDetails trailingStopDetails;

    public MarketOrder(OrderType type, InstrumentName name, DecimalNumber units, TimeInForce timeInForce, TakeProfitDetails takeProfitDetails, StopLossDetails stopLossDetails, TrailingStopLossDetails trailingStopDetails) {
        this.type = type;
        this.name = name;
        this.units = units;
        this.timeInForce = timeInForce;
        this.takeProfitDetails = takeProfitDetails;
        this.stopLossDetails = stopLossDetails;
        this.trailingStopDetails = trailingStopDetails;
    }
}
