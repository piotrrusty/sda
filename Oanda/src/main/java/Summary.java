import com.oanda.v20.Context;
import com.oanda.v20.account.AccountID;
import com.oanda.v20.account.AccountSummary;

public class Summary {

    public static void main(String[] args) {
        Context ctx = new Context(
                "https://api-fxpractice.oanda.com",
                "4cbcfb1f3fadd955a96d30d6027653b8-41a4ec28f0c1e8a18a97f3a5896f1214");

        try {
            AccountSummary summary = ctx.account.summary(
                    new AccountID("101-004-8211023-001")).getAccount();
            System.out.println(summary);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

