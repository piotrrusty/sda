package pl.sda.rejestrobywateli;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RejestrObywateli {

    Map<String, Obywatel> rejestr = new HashMap<>();

//    public void dodajObywatelaDoRejestru (Obywatel obywatel){
//        rejestr.put(obywatel.pesel, obywatel);
//    }

    public void dodajObywatela (String pesel, String imie, String nazwisko){
        Obywatel obywatel = new Obywatel(pesel, imie, nazwisko);
        rejestr.put(pesel, obywatel);
    }

    public List<Obywatel> znajdzObywateliUrodzonychPrzed(int rok){
        return rejestr.values().stream()
                .filter(obywatel -> parsePeselToDate(obywatel.getPesel()).getYear() < rok )
                .collect(Collectors.toList());
    }

    public List<Obywatel> znajdzObywateliZRokuZImieniem (String imie, int rok){
        return rejestr.values().stream()
                .filter(obywatel -> parsePeselToDate(obywatel.getPesel()).getYear() == rok )
                .filter(obywatel -> obywatel.getImie().toLowerCase().equals(imie))
                .collect(Collectors.toList());
    }

    public List<Obywatel> znajdzObywatelaPoNazwisku (String nazwisko){
        return rejestr.values().stream()
                .filter(obywatel -> obywatel.getNazwisko().equals(nazwisko))
                .collect(Collectors.toList());
    }

    public Optional<Obywatel> znajdzObywatelaPoPeselu (String pesel){
        return rejestr.values().stream()
                .filter(obywatel -> obywatel.getPesel().equals(pesel))
                .findFirst();
    }

    @Override
    public String toString() {
        return "RejestrObywateli{" +
                "rejestr=" + rejestr +
                '}';
    }

    private LocalDate parsePeselToDate(String pesel){
        if(Integer.parseInt(pesel.substring(2,4)) >= 20){
            return LocalDate.of(2000+Integer.parseInt(pesel.substring(0,2)),Integer.parseInt(pesel.substring(2,4))-20,Integer.parseInt(pesel.substring(4,6)));
        }
        return LocalDate.of(1900+Integer.parseInt(pesel.substring(0,2)),Integer.parseInt(pesel.substring(2,4)),Integer.parseInt(pesel.substring(4,6)));
    }
}
