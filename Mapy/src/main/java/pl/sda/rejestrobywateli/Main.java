package pl.sda.rejestrobywateli;

public class Main {
    public static void main(String[] args) {
        Obywatel obywatel1 = new Obywatel("86032703717", "Piotr", "Rdzanek");

        RejestrObywateli rejestr = new RejestrObywateli();

        rejestr.dodajObywatelaDoRejestru(obywatel1);

//        System.out.println(rejestr);

        rejestr.dodajObywatela("46327572834", "Anna", "Cykpyk");
        rejestr.dodajObywatela("89246324233", "Johnny", "Bravo");
        rejestr.dodajObywatela("67243487836", "Roman", "Romek");
        rejestr.dodajObywatela("98765431245", "Kasia", "Srasia");

//        System.out.println(rejestr);

        int rok = 1998;

        String szukana = Integer.toString(rok);
//        szukana = String.valueOf(szukana.split("\\d[2]"));

        System.out.println(szukana);

        rejestr.znajdzObywateliUrodzonychPrzed(1998);
    }
}
