package pl.sda.sklep;

public class Produkt {

    String kodKreskowyProduktu;
    double cenaProduktu;
    String opisProduktu;

    public Produkt(String kodKreskowyProduktu, double cenaProduktu, String opisProduktu) {
        this.kodKreskowyProduktu = kodKreskowyProduktu;
        this.cenaProduktu = cenaProduktu;
        this.opisProduktu = opisProduktu;
    }

    @Override
    public String toString() {
        return opisProduktu +"  "+cenaProduktu;
    }

    public void wypiszOpisICene (){
        System.out.println(this.opisProduktu+" - "+this.cenaProduktu+" PLN");
    }
}
