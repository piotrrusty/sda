package pl.sda.sklep;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sklep {

    private Map<String, Produkt> oferta= new HashMap<>();


    public Sklep() {
    }

    public void dodajProduktDoOferty (Produkt produkt){
        oferta.put(produkt.kodKreskowyProduktu, produkt);
    }

    public Map<String, Produkt> getOferta() {
        return oferta;
    }

    public void wypiszDaneProduktu(String kodKreskowy){
        if(oferta.containsKey(kodKreskowy)){

        oferta.get(kodKreskowy).wypiszOpisICene();
        }else{
            System.out.println("Niezarejestrowany produkt!");
        }
    }
}
