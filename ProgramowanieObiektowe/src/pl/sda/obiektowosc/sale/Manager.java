package pl.sda.obiektowosc.sale;

public class Manager {

    Sala[] zarzadzaneSale;
    String imie;

    public void wyswietlDostepneSale() {
        System.out.println("Dostępne sale:");
        for (Sala sala : zarzadzaneSale) {
            if (sala.czyJestWolna) {
                sala.wyswietlOpisSali();
            }
        }


    }

    public void zabookujSale(String nazwaSali){
        for (Sala sala:zarzadzaneSale){
            if (sala.nazwa.equals(nazwaSali)){
                if (sala.czyJestWolna){
                    System.out.println("Bookuje sale "+nazwaSali);
                    sala.czyJestWolna=false;
                }else{
                    System.out.println("Sala "+nazwaSali+ " nie jest dostępna.");
                }
            }
        }
}
}
