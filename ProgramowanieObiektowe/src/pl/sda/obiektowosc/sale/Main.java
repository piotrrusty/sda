package pl.sda.obiektowosc.sale;

public class Main {
    public static void main(String[] args) {
        Sala sda = new Sala();
        sda.nazwa = "SDA";
        sda.czyJestRzutnik = true;
        sda.iloscStanowisk = 18;
        sda.m2 = 40.55;

        Sala gdansk = new Sala();
        gdansk.nazwa = "Gdansk";
        gdansk.czyJestRzutnik = true;
        gdansk.iloscStanowisk = 20;
        gdansk.m2 = 150.00;

        Sala dublin = new Sala("Dublin", 55.55, 15, false );
        Sala krakow = new Sala("Krakow", 78.45, 23, true);

        Sala[] sale = new Sala[]{sda, gdansk, dublin, krakow};

        Manager jan = new Manager();
        jan.imie = "Jan";
        jan.zarzadzaneSale = sale;

        jan.wyswietlDostepneSale();
        jan.zabookujSale("Gdansk");
        jan.zabookujSale("Gdansk");
        System.out.println("Po zabookowaniu");
        jan.wyswietlDostepneSale();
    }
}
