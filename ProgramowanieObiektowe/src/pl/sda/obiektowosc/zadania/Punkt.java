package pl.sda.obiektowosc.zadania;

public class Punkt {
    int x;
    int y;

    public Punkt(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void wyswietl(){
        System.out.println(String.format("(%d,%d)", x, y));
    }

    public double obliczOdleglosc(Punkt punkt){
        double odleglosc = Math.sqrt(((this.x-punkt.x)*(this.x-punkt.x))+((this.y-punkt.y)*(this.y-punkt.y)));
        System.out.println(odleglosc);
        return odleglosc;
    }
}
