package pl.sda.obiektowosc.zadania;

import java.util.Random;
import java.util.Scanner;

public class Gra {
    private int tajneHasło;

    public Gra(){
        Random random = new Random();
        tajneHasło = 1 + random.nextInt(99);
    }

    public void graj(){
        int podaneHasło;
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Podaj liczbę");
            podaneHasło = scanner.nextInt();
            if (podaneHasło>tajneHasło){
                System.out.println("Podana liczba jest za duża");
            }else if (podaneHasło<tajneHasło){
                System.out.println("Podana liczba jest za mała");
            }
        }while (podaneHasło!=tajneHasło);
        System.out.println("Gratulacje, odgadłeś tajne hasło: "+tajneHasło);
    }
}