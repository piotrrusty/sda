package pl.sda.obiektowosc.zadania;

public class Ulamek {
    private int licznik;
    private int mianownik;
    private int calosci;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
        uproscUlamek();
    }

    private void uproscUlamek() {
        int nwd;

        calosci=licznik/mianownik;
        licznik-=calosci*mianownik;

        do {
            nwd = obliczNWD(licznik, mianownik);
            this.licznik = licznik / nwd;
            this.mianownik = mianownik / nwd;
        } while (nwd != 1);

    }

    private int obliczNWD(int licznik, int mianownik) {
        int nwd=1;
        if (licznik<=mianownik){
            for (int i=1; i<=licznik; i++){
                if (licznik%i==0 && mianownik%i==0){
                    nwd=i;
                }
            }
        }else{
            for (int i=1; i<=mianownik; i++){
                if (licznik%i==0 && mianownik%i==0){
                    nwd=i;
                }
            }
        }return nwd;
    }

    public void wyswietl() {
        System.out.println(String.format("%d i %d/%d", calosci, licznik, mianownik));
    }

    public static void main(String[] args) {
        Ulamek polowka = new Ulamek(1, 2);
        polowka.wyswietl();

        Ulamek cwiartka = polowka.pomnoz(polowka);
        cwiartka.wyswietl();

        Ulamek osemka = polowka.pomnoz(cwiartka);
        osemka.wyswietl();

        polowka.pomnoz(polowka).pomnoz(cwiartka).pomnoz(osemka).pomnoz(polowka).wyswietl();

        cwiartka.dodaj(osemka).wyswietl();

        Ulamek jakis = polowka.dodaj(osemka);
        jakis.wyswietl();

        Ulamek podzielony = polowka.podziel(polowka);
        podzielony.wyswietl();
    }

    public Ulamek pomnoz(Ulamek ulamek) {

        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        Ulamek wynik = new Ulamek(licznik, mianownik);
        return wynik;

    }

    public Ulamek dodaj(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik + ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek podziel(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik;
        int mianownik = this.mianownik * ulamek.licznik;

        return new Ulamek(licznik, mianownik);
    }
}
