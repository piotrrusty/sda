package pl.sda.obiektowosc.zadania;

import java.util.Scanner;

public class PunktPlansza {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj współrzędną x pierwszego punktu");
        int x1 = input.nextInt();
        System.out.println("Podaj współrzędną y pierwszego punktu");
        int y1 = input.nextInt();
        Punkt punkt1 = new Punkt(x1, y1);

        System.out.println("Podaj współrzędną x drugiego punktu");
        int x2 = input.nextInt();
        System.out.println("Podaj współrzędną y drugiego punktu");
        int y2 = input.nextInt();
        Punkt punkt2 = new Punkt(x2, y2);

        System.out.println();
        punkt1.wyswietl();
        punkt2.wyswietl();

        System.out.println("Odległość punktu pierwszego do drugiego to");
        punkt1.obliczOdleglosc(punkt2);

    }
}
