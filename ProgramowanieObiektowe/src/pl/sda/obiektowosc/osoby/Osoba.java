package pl.sda.obiektowosc.osoby;

import java.util.Arrays;

public class Osoba {
    String imie;
    int rokUrodzenia;
    char plec;

    public Osoba(String imie, int rokUrodzenia, char plec) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
        this.plec = plec;
    }

    public Osoba() {

    }

    public static void main(String[] args) {
        Osoba ania = new Osoba();
        Osoba andrzej = new Osoba();
        Osoba mariola = new Osoba();

        ania.imie = "Ania";
        ania.rokUrodzenia = 1993;
        ania.plec = 'k';
        ania.przedstawSie();

        andrzej.imie = "Andrzej";
        andrzej.rokUrodzenia = 1964;
        andrzej.plec = 'm';
        andrzej.przedstawSie();

        mariola.imie = "Mariola";
        mariola.rokUrodzenia = 1950;
        mariola.plec = 'k';
        mariola.przedstawSie();

        Osoba piotr = new Osoba("Piotr", 1986, 'm');
        Osoba marian = new Osoba("Marian", 1974, 'm');
        Osoba ula = new Osoba("Urszula", 1989, 'k');
        Osoba zdzichu = new Osoba("Zdzisław", 1940, 'm');

        Osoba[] ludzie = new Osoba[]{ania, andrzej, mariola, piotr, marian, ula, zdzichu};

                System.out.println("KOBIETY");
        for (Osoba osoba : ludzie) {
            if (osoba.plec=='k') {
                System.out.println(osoba.imie);
            }
        }
                System.out.println("MĘŻCZYŹNI");
        for (Osoba osoba : ludzie) {
            if (!osoba.imie.endsWith("a")) {
                System.out.println(osoba.imie);
            }
        }


    }

    private void przedstawSie() {
        int wiek = 2018 - rokUrodzenia;
        System.out.println("Cześć, mam na imię " + imie + " i mam " + wiek + " lat.");
    }
}
