package pl.sda.obiektowosc.bank;


public class KontoSlack {

    public long numerKonta;
    public int stanKonta;

    public KontoSlack(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public static void main(String[] args) {
        KontoSlack kontoAndrzeja = new KontoSlack(123L, 1000);
        KontoSlack kontoBeaty = new KontoSlack(555L, 2000);

        System.out.println("Przelew od Andrzeja do Beaty na kwotę 200");
        System.out.println("Przed przelewem");
        kontoAndrzeja.wyswietlStanKonta();
        kontoBeaty.wyswietlStanKonta();
        wykonajPrzelew(kontoAndrzeja, kontoBeaty, 200);
        System.out.println("Po przelewie");
        kontoAndrzeja.wyswietlStanKonta();
        kontoBeaty.wyswietlStanKonta();
    }

    public void wyswietlStanKonta() {
        System.out.println(stanKonta);
    }

    public void wplacSrodki(int kwota) {
        stanKonta += kwota;
    }

    public int pobierzSrodki(int kwota) {
        if (kwota > stanKonta) {
            System.out.println("Za malo srodkow");
            return 0;
        } else {
            stanKonta -= kwota;
            return kwota;
        }
    }

    public static void wykonajPrzelew(KontoSlack nadawca, KontoSlack odbiorca,
                                      int kwota) {
        int srodki = nadawca.pobierzSrodki(kwota);
        odbiorca.wplacSrodki(srodki);
    }
}
