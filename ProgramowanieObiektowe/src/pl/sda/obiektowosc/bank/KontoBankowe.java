package pl.sda.obiektowosc.bank;

import java.util.Scanner;

public class KontoBankowe {
    private long numerKonta;
    private int stanKonta;

    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public void wyswietlStanKonta() {
        System.out.println(stanKonta);
    }

    public void wplacSrodki(int kwota) {
        stanKonta += kwota;
    }

    public int pobierzSrodki(int kwota) {
        Scanner input = new Scanner(System.in);
        System.out.println("Ile środków chcesz pobrać?");
        int wyplata = input.nextInt();
        this.stanKonta = stanKonta - wyplata;
        return stanKonta;
    }

    public static void main(String[] args) {
        KontoBankowe andrzej = new KontoBankowe(123L, 1000);
        KontoBankowe beata = new KontoBankowe(555L, 2000);

        System.out.println("Wybierz co chcesz zrobić");
        System.out.println("Wpisz, 's' by wyświetlić stan konta, 'p' by pobrać środki, 'w' by wplacic srodki");
        Scanner input = new Scanner(System.in);
        String dzialanie = input.nextLine();
        int wplata;
        if(dzialanie.equals("s")){
            andrzej.wyswietlStanKonta();
        }
        if(dzialanie.equals("p")){
            System.out.println("Ile pieniędzy chcesz pobrać?");
        }
        if(dzialanie.equals("w")){
            System.out.println("Ile pieniedzy chcesz wplacic?");
            wplata = input.nextInt();
            andrzej.wplacSrodki(wplata);
        }
    }
}
