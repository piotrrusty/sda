package pl.sda.obiektowosc.bank;

public class Main {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(2222, 3000);
        KontoBankowe kontoCwaniaczka = new KontoBankowe(1111, 0);

//        kontoCwaniaczka.stanKonta = 1000000;

        kontoAndrzeja.wyswietlStanKonta();
        kontoCwaniaczka.wyswietlStanKonta();
    }
}
