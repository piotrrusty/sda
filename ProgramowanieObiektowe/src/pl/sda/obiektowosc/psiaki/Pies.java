package pl.sda.obiektowosc.psiaki;

public class Pies {
    String imie;
    int rokUrodzenia;
    String kolor;

    public static void main(String[] args) {
        Pies burek = new Pies();
        burek.imie = "Burek";
        burek.rokUrodzenia = 2015;
        burek.kolor = "Czarny";

        System.out.println("Burek");
        burek.dajGlos();

        System.out.println("Mietek");
        Pies corgi = new Pies();
        corgi.imie = "Mietek";
        corgi.rokUrodzenia = 2017;
        corgi.kolor = "Rudy";
        corgi.dajGlos();
    }

    private void dajGlos() {
        int wiek = 2018 - rokUrodzenia;
        for (int i = 0; i < wiek; i++) {

            System.out.println("Hau!");
        }
    }
}
