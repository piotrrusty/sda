package pl.sda.obiektowosc.superbohater;

public class SuperBohater {

    private String nazwa;
    private String supermoc;

    public SuperBohater(String nazwa, String supermoc) {
        this.nazwa = nazwa;
        this.supermoc = supermoc;
    }

    public static void main(String[] args) {

        SuperBohater johnny = new SuperBohater("Johnny", "Ogień");
        SuperBohater spiderman = new SuperBohater("Spiderman", "Pajęczyna");
        SuperBohater hulk = new SuperBohater("Hulk", "Siła");

        johnny = spiderman;
        johnny = null;
        spiderman = null;

        System.out.println(spiderman==null);

        johnny=null;
        spiderman=johnny;
        johnny=hulk;
        System.out.println(johnny==null);
        System.out.println(spiderman==null);
        System.out.println(hulk==null);

        String imie = "Dexter";
        String imie2 = "Dexter";
        String imie3 = new String("Basia");
        String imie4 = new String("Basia");


        System.out.println("imie3==imie4");
        System.out.println(imie3==imie4);
        System.out.println("imie3equals(imie4)");
        System.out.println(imie3.equals(imie4));
        System.out.println("imie==imie2");
        System.out.println(imie==imie2);
        System.out.println("imie.equals(imie2)");
        System.out.println(imie.equals(imie2));
    }
}
