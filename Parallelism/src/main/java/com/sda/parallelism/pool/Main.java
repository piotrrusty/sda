package com.sda.parallelism.pool;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main {
    public static void main(String[] args) {
        ExecutorService pulaWątków = Executors.newFixedThreadPool(5);
        boolean czyPętlaPracuje = true;
        Scanner sc = new Scanner(System.in);
        while (czyPętlaPracuje) {
            final int ileCzasuSpać = sc.nextInt();
            pulaWątków.submit(new Runnable() {
                public void run() {
                    System.out.println("Start wątku: " + ileCzasuSpać);
                    try {
                        Thread.sleep(ileCzasuSpać);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Koniec wątku: " + ileCzasuSpać);
                }
            });
        }
    }
}


