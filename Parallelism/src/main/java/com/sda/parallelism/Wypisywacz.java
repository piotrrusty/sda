package com.sda.parallelism;

public class Wypisywacz implements Runnable {

    private int ileRazyWypisac;

    public Wypisywacz(int ileRazyWypisac){
        this.ileRazyWypisac = ileRazyWypisac;
    }

    public void run() {
        for(int i = 0; i<ileRazyWypisac; i++){
            System.out.println("Hello World");
        }

    }
}
