package pl.sda.enumy;

import java.util.Scanner;

public class MainBilety {

    public static void main(String[] args) {
        Bilet bilet = Bilet.ULGOWY_GODZINNY;
        Bilet bilet2 = Bilet.ULGOWY_GODZINNY;
        Bilet bilet3 = Bilet.ULGOWY_CALODNIOWY;
        Bilet bilet4 = Bilet.NORMALNY_CALODNIOWY;
        Bilet bilet5 = Bilet.NORMALNY_GODZINNY;


        System.out.println(bilet.pobierzCeneBiletu());
        System.out.println(bilet2.pobierzCeneBiletu());
        bilet.wyswietlDaneBiletu();
        bilet4.wyswietlDaneBiletu();

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj swój wiek");
        int wiek = input.nextInt();
        System.out.println("Podaj czas jazdy");
        int czas = input.nextInt();
        System.out.println("Podaj ile masz pieniędzy");
        double kwota = input.nextDouble();

        Bilet.wyliczBilet(wiek, czas, kwota).wyswietlDaneBiletu();
    }
}
