package pl.sda.enumy;

public enum Bilet {
    ULGOWY_GODZINNY(1.6, 60),
    ULGOWY_CALODNIOWY(5.2, 1440),
    NORMALNY_GODZINNY(3.2, 60),
    NORMALNY_CALODNIOWY(10.4, 1440),
    BRAK_BILETU(0, 0);

    private double cena;
    private int czas;

    Bilet(double cena, int czas) {
        this.cena = cena;
        this.czas = czas;
    }

    public double pobierzCeneBiletu() {
        return cena;
    }

    public int pobierzCzasJazdy() {
        return czas;
    }

    public void wyswietlDaneBiletu() {
        String nazwa = this.toString();
        nazwa = nazwa.toLowerCase().replace("_", " ");

        System.out.println("Bilet " + nazwa + " - " + cena + " PLN");
    }

    public static Bilet wyliczBilet(int wiek, int czas, double kwota) {

        Bilet kupionyBilet= Bilet.BRAK_BILETU;

        if (wiek > 18 && czas > 60) {
            kupionyBilet = NORMALNY_CALODNIOWY;
        }
        if (wiek <= 18 && czas > 60) {
            kupionyBilet = ULGOWY_CALODNIOWY;
        }
        if (wiek > 18 && czas <= 60) {
            kupionyBilet = NORMALNY_GODZINNY;
        }
        if (wiek <= 18 && czas <= 60) {
            kupionyBilet = ULGOWY_GODZINNY;
        }

        if (kwota < kupionyBilet.cena) {
            return Bilet.BRAK_BILETU;
        }
        return kupionyBilet;
    }


}
