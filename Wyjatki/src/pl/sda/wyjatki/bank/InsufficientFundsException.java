package pl.sda.wyjatki.bank;

public class InsufficientFundsException extends Exception {
    public InsufficientFundsException() {
        super("Próbujesz wypłacić więcej niż masz na koncie");
    }
}

