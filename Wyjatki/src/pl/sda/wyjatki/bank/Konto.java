package pl.sda.wyjatki.bank;

public class Konto {
    private final String imie;
    private int stanKonta;

    public Konto(String imie, int kwotaStartowa) {
        this.imie = imie;
        this.stanKonta = kwotaStartowa;
    }

    public int wybierzPieniadze(int kwota) throws InsufficientFundsException {
        if (kwota > this.stanKonta) {
            throw new InsufficientFundsException();
        } else {
            this.stanKonta -= kwota;
            return stanKonta;
        }
    }

    public int pobierzStanKonta() {
        return stanKonta;
    }
}


