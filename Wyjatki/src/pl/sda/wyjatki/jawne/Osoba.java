package pl.sda.wyjatki.jawne;

public class Osoba {

    String imie;
    int rokUrodzenia;

    public Osoba(String imie, int rokUrodzenia) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
    }

    public static int obliczWiekOsobyWRoku(Osoba osoba, int wybranyRok) throws Exception {
        int wiek = wybranyRok - osoba.rokUrodzenia;
        if(wiek<0) {
            throw new Exception("Wybrany rok mniejszy od roku urodzenia!");
        }else{
            return wiek;
        }
    }
}
