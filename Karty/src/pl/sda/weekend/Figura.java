package pl.sda.weekend;

public enum Figura {
    AS,
    KRÓL,
    DAMA,
    WALET,
    DZIESIĘĆ,
    DZIEWIĘĆ,
    OSIEM,
    SIEDEM,
    SZEŚĆ,
    PIĘĆ,
    CZTERY,
    TRZY,
    DWA;
}
