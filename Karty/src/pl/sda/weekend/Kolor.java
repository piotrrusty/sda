package pl.sda.weekend;

public enum Kolor {
    PIK('♠'),
    KIER('♥'),
    KARO('♦'),
    TREFL('♣');

    private final char znak;

    Kolor(char znak) {
        this.znak = znak;
    }


    @Override
    public String toString() {
        return String.valueOf(znak);
    }
}
