package pl.sda.weekend;

public class Stol {
    public static void main(String[] args) {
        Karta[] karty = new Karta[52];

        int wydrukowaneKarty=0;

        for (Figura figura:Figura.values()){
            for (Kolor kolor:Kolor.values()) {
                karty[wydrukowaneKarty] = new Karta(figura, kolor);

                wydrukowaneKarty++;
            }
        }

        for (Karta karta: karty) {
            System.out.println(karta);
        }
    }
}
