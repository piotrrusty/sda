import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by krzysztof on 12.11.17.
 */


public class LotteryResults {

    @EJB
    private NumberGenerator numberGenerator;

    @POST
    @Path("/results")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getLotteryResults(List<Integer> userNumbers) {

        boolean resultSucces = isSuccess(userNumbers, numberGenerator.getWinningNumbers());

        return Response.ok(resultSucces).build();
    }

    /**
     * Implmentację tej metody można oprzeć na zwykłej pętli, lub API JAVA8 (stream, filter, collect, Collectors, toList())
     * W obu przypadkach przyda się metoda contains z interfejsu List
     *
     * @param numbers
     * @param winningNumbers
     * @return
     */
    private boolean isSuccess(List<Integer> numbers, List<Integer> winningNumbers) {

        winningNumbers.removeAll(numbers);
        return winningNumbers.size()<6;
    }

}
