import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.*;

/**
 * Created by krzysztof on 12.11.17.
 */

@Singleton
@Startup
public class NumberGenerator {

    private static final Logger logger = Logger.getLogger(NumberGenerator.class);

    private List<Integer> winningNumbers;

    /**
     * W implementacji tej metody można posłużyć się klasą java.util.Random
     * @return
     */

    private List<Integer> drawWinningNumbers() {

        Set<Integer> numbers = new HashSet<>();
        while (numbers.size()<6) {
            numbers.add(new Random().nextInt(49+1));
        }

        return new ArrayList<>(numbers);
    }

    @PostConstruct
    public void generateNumbersOnStart() {
        winningNumbers = drawWinningNumbers();

        logger.info("Numbers: " + winningNumbers);

    }

    public List<Integer> getWinningNumbers() {
        return winningNumbers;
    }
}
