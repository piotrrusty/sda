package pl.sda.wiedza.zadania;

public class TypyDanych {
    public static void main(String[] args) {
        String tekst = "SUPER";
        char litera = 'g';
        int liczba = 456;

        liczba = 54;
        liczba = 356;

        System.out.println(litera);

        System.out.println(liczba + (litera + tekst));
        System.out.println(liczba + ((int)litera + tekst));
        System.out.println("Kod ASCII litery '" + litera + "' to: " + (int)litera);
//
//        System.out.println(liczba + 'g' + tekst);
//        System.out.println(litera+liczba+tekst);
//
//        System.out.println();
    }
}
