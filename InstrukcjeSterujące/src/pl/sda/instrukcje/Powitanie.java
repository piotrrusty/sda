package pl.sda.instrukcje;


import java.util.Scanner;

public class Powitanie {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj swoje imie.");
        String imie = input.nextLine();

        switch (imie){
            case "Ania":
                System.out.println("Cześć Ania, kopę lat");
                break;
            case "Beata":
                    System.out.println("Cześć Beata, co słychać?");
                    break;
            case "Janusz":
                        System.out.println("Cześć. Jak Twój passat?");
                        break;
            default:
                System.out.println("Cześć " + imie + " miło Cię poznać.");
        }
    }
}
