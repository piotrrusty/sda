package pl.sda.petle.pfor;

import java.util.Scanner;

public class DrukujLiczbyZadanie {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbe:");
        int liczba = input.nextInt();
        System.out.println("Podaj dzielnik:");
        int dzielnik = input.nextInt();

        for(int i=0; i<liczba; i++){
            if(i%dzielnik==0){
                System.out.println(i);
            }
        }
    }
}
