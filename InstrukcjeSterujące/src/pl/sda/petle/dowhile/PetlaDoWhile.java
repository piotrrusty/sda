package pl.sda.petle.dowhile;

import java.util.Scanner;

public class PetlaDoWhile {
    public static void main(String[] args) {

        System.out.println("Podaj liczbe");

        Scanner input = new Scanner(System.in);
        int liczba = input.nextInt();

        int licznik=0;
        int suma=0;


        do {
            suma+=licznik;
            licznik++;
        }while (licznik<=liczba);
        System.out.println(suma);
    }
}
