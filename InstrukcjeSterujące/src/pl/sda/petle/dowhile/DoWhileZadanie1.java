package pl.sda.petle.dowhile;

import java.util.Scanner;

public class DoWhileZadanie1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczba;


        do {
            System.out.println("Podaj liczbe");
            liczba = input.nextInt();
            for (int i = 0; i <= liczba; i++) {
                System.out.println(i);
            }

        } while (liczba > 0);
    }
}
