package pl.sda.petle.dowhile;

import java.util.Scanner;

public class HelloHello {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int licznik;

        do {
            System.out.println("Podaj liczbe");
            licznik = input.nextInt();
            for (int i = 0; i < licznik; i++) {
                System.out.println("Hello World");
            }
        } while (licznik!=0);

    }
}
