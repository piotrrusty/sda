package pl.sda.petle.pwhile;

import java.util.Scanner;

public class PetlaWhileZadanie2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj liczbe:");

        int liczba = input.nextInt();

        int wynik = 1;
        int licznik= 1;

        while(licznik<=liczba){
            wynik=licznik*wynik;
            licznik++;
        }
        System.out.println(wynik);
    }
}
