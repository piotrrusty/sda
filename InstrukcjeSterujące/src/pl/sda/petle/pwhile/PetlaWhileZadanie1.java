package pl.sda.petle.pwhile;

import java.util.Scanner;

public class PetlaWhileZadanie1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj Liczbę:");

        int liczba = input.nextInt();

        int licznik=0;
        int wynik=0;
        while(licznik<=liczba) {
            wynik = wynik+licznik;
            licznik++;
        }
        System.out.println(wynik);
    }
}
