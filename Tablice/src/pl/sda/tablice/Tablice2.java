package pl.sda.tablice;

import java.util.Arrays;

public class Tablice2 {
    public static void main(String[] args) {
        int[] liczby = new int[]{1, 3, 5, 10, 12, 17, 20, 34, 234};
//int[] liczby = new int[0];
        System.out.println(liczby[3]);
        System.out.println(Arrays.toString(liczby));

        for (int i = 0; i < liczby.length; i++) {
            System.out.println(liczby[i]);
        }

        for (int i = 0; i < liczby.length; i++) {
            if (i % 2 == 0) {
                System.out.println(liczby[(i)]);
            }
        }


        for (int liczba : liczby) {
            if (liczba % 2 == 0) {
                System.out.println(liczba);
            }
        }

        for (int i = liczby.length-1; i >=0; i--) {
            System.out.println(liczby[i]);
        }

    }
}
