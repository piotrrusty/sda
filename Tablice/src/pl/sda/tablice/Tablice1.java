package pl.sda.tablice;

import java.util.Arrays;

public class Tablice1 {
    public static void main(String[] args) {
//        String[] imiona = new String[3];
//        imiona[2] = "Piotr";
//        imiona[0] = "Ania";

        String[] imiona = new String[] {"Ania", "Piotr", "Mateusz", "Milena", "Zenon", "Lukasz", "Igor"};


//        for(int i=0; i<imiona.length; i++){
//            System.out.println(imiona[i]);
//        }

        imiona[2] = "Jacek";

        Arrays.sort(imiona);

        System.out.println("Tylko Kobiety");
        for (String imie : imiona) {
            if (imie.endsWith("a")){
                System.out.println(imie);
            }
        }
        System.out.println("Tylko Panowie");
        for (String imie : imiona) {
            if (!imie.endsWith("a")){
                System.out.println(imie);
            }
        }
    }
}
