package pl.sda.obiektowosc;

public class Wyscig {
    public static void main(String[] args) {
        Zawodnik nr1 = new Zawodnik("Marian", 198, 27, 50);
        Zawodnik nr2 = new Zawodnik("Johnny", 258, 20, 57);
        Zawodnik nr3 = new Zawodnik("Angela", 555, 22, 55);
        System.out.println("przedstawienie");
        nr1.przedstawSie();
        nr2.przedstawSie();
        nr3.przedstawSie();

        Zawodnik[] gracze = new Zawodnik[]{nr1,nr2,nr3};

        Zawodnik zwyciezca=null;

        while (zwyciezca==null){
            for (Zawodnik zawodnik:gracze){
                zawodnik.biegnij();
                if (zawodnik.distance>=50000){
                    System.out.println("Zwyciezca zostal "+zawodnik.imie);
                    zwyciezca = zawodnik;
                    break;
                }
            }
        }
    }
}
