package pl.sda.obiektowosc;

import java.util.Random;

public class Zawodnik {

    public String imie;
    public int identyfikator;
    public int vMin;
    public int vMax;
    public int distance = 0;

    public Zawodnik(String imie, int identyfikator, int vMin, int vMax) {
        this.imie = imie;
        this.identyfikator = identyfikator;
        this.vMin = vMin;
        this.vMax = vMax;
    }

    public void przedstawSie() {
        System.out.println("Cześć, mam na imię " + imie + " mam numer " + identyfikator + ", biegam z prędkością od " + vMin + " do " + vMax);
    }

    public void biegnij() {
        Random random = new Random();
        int distance = random.nextInt(vMax - vMin) + vMin;
        this.distance += distance;
    }
}
