package pl.sda.abstrakcja;

public class SymulatorFarby {

    public static int obliczZapotrzebowanieNaFarbe(Figura[] figury, double wielkoscPojemnika) {

        double poleSuma=0;
        for (Figura f : figury) {
            poleSuma += f.obliczPole();
        }

        int iloscPojemnikow = (int) (poleSuma / wielkoscPojemnika);
        if (poleSuma%wielkoscPojemnika!=0){
            iloscPojemnikow++;
        }
        return iloscPojemnikow;
    }


    public static void main(String[] args) {
        Kwadrat kwadrat1 = new Kwadrat(5);
        Kwadrat kwadrat2 = new Kwadrat(7);
        Prostokat prosto1 = new Prostokat(3, 7);
        Okreg okreg1 = new Okreg(5.34);

        Figura[] figury = new Figura[] {kwadrat1, kwadrat1, kwadrat1, prosto1, okreg1, kwadrat2};

        SymulatorFarby symulator = new SymulatorFarby();

//        symulator.obliczZapotrzebowanieNaFarbe(figury, 60);
        System.out.println(symulator.obliczZapotrzebowanieNaFarbe(figury, 100));
    }
}
