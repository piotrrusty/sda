package pl.sda.abstrakcja;

public abstract class Figura {

    public abstract double obliczObwod();
    public abstract double obliczPole();
}
