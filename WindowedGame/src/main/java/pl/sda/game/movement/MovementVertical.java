package pl.sda.game.movement;

public enum MovementVertical {
    NONE, UP, DOWN
}
