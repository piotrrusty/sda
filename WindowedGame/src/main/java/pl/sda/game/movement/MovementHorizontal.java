package pl.sda.game.movement;

public enum MovementHorizontal {
    NONE, LEFT, RIGHT
}
