package pl.sda.game.views;

import pl.sda.game.map.MapReader;
import pl.sda.game.models.AbstractPaintable;

import java.io.IOException;
import java.util.List;

import pl.sda.game.models.GameHero;
import pl.sda.game.movement.MovementHorizontal;
import pl.sda.game.movement.MovementVertical;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {
    private Dimension wymiar;

    private GameHero hero = new GameHero(400, 300);
    private MovementHorizontal horizontal;
    private MovementVertical vertical;

    private List<AbstractPaintable> przeszkody;

    public MainPanel(Dimension wymiar) {
        super();

        setSize(wymiar);
        setPreferredSize(wymiar);
        setMinimumSize(wymiar);
        setMaximumSize(wymiar);

        this.wymiar = wymiar;

        MapReader reader = new MapReader("mapy/level1.map");
        try {
            przeszkody = reader.loadMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D pióro = (Graphics2D) g;
        pióro.setColor(Color.GREEN);
        pióro.fillRect(0, 0, wymiar.width, wymiar.height);
        hero.paint(pióro);

        for (AbstractPaintable paintable : przeszkody) {
            paintable.paint(pióro);
        }
    }

    public void moveObjectsOnScene() {
        hero.move(vertical, horizontal);
    }

    public void verticalMovement(MovementVertical direction) {
        vertical = direction;
    }

    public void horizontalMovement(MovementHorizontal direction) {
        horizontal = direction;
    }
}
