package pl.sda.game.map;

import pl.sda.game.models.AbstractPaintable;
import pl.sda.game.models.ObstacleGround;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapReader {
    private String mapName;
    private BufferedReader reader;

    public MapReader(String mapName) {
        this.mapName = mapName;
    }

    private void openFile(){
        try{
            reader = new BufferedReader(new FileReader(mapName));
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<AbstractPaintable> loadMap() throws IOException {
        openFile();

        String linia = null;
        int licznikLinii = 0;
        List<AbstractPaintable> elementyMapy = new ArrayList<>();
        while((linia = reader.readLine()) != null){
            for(int i = 0; i<linia.length(); i++){
                if(linia.charAt(i) == '1'){
                    elementyMapy.add(new ObstacleGround(600, i, licznikLinii));
                }
            }
            licznikLinii++;

        }
        return elementyMapy;
    }
}
