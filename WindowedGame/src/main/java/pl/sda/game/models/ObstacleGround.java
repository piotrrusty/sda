package pl.sda.game.models;

import java.awt.*;

public class ObstacleGround extends AbstractPaintable {
    private final static int sizeX = 30;
    private final static int sizeY = 30;

    public ObstacleGround(int positionYOffset, int positionX, int positionY) {
        super(positionX*sizeX, positionYOffset - positionY*sizeY);
    }

    public void paint(Graphics2D pióro) {
        pióro.setColor(Color.BLACK);
        pióro.fillRect(positionX, positionY, sizeX, sizeY);

    }
}
