package pl.sda.powtorka.zad2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Parking p = new Parking(List.of(new MiejsceParkingowe(1, StanMiejsca.WOLNE), new MiejsceParkingowe(2, StanMiejsca.WOLNE)), true);
        MiejsceParkingowe miejsce3 = new MiejsceParkingowe(3, StanMiejsca.ZAJETE);
//        p.listaMiejscParkingowych.add(miejsce3);


//        System.out.println(p.sprawdzZajetoscMiejsca(2));

//        p.zajmijMiejsce(1);
//        System.out.println(p.zajmijMiejsce(1));
        System.out.println(p.listaMiejscParkingowych.get(0).stanMiejsca);
        System.out.println(p.zajmijMiejsce(1));
        System.out.println(p.zajmijMiejsce(1));
    }
}
