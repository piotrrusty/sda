package pl.sda.powtorka.zad2;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class MiejsceParkingowe {

    int numerMiejsca;
    StanMiejsca stanMiejsca;
}
