package pl.sda.powtorka.zad2;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
public class Parking {

    List<MiejsceParkingowe> listaMiejscParkingowych = new ArrayList<>();
    boolean stanParkingu;

    boolean sprawdzZajetoscMiejsca(int nrMiejsca){
        for (MiejsceParkingowe m: listaMiejscParkingowych) {
            if(nrMiejsca==m.numerMiejsca){
                if(m.stanMiejsca == StanMiejsca.WOLNE) {
                    return true;
                }
            }

        }
        return false;
    }

    boolean zajmijMiejsce(int nrMiejsca){
        for (MiejsceParkingowe m: listaMiejscParkingowych){
            if(nrMiejsca==m.getNumerMiejsca()){
                if(m.getStanMiejsca()==StanMiejsca.WOLNE){
                    m.setStanMiejsca(StanMiejsca.ZAJETE);
                    return true;
                }
            }
        }
        return false;
    }


}
