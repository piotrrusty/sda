package pl.sda.powtorka.zad1;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Student {

    long nrIndeksu;
    String imie;
    String nazwisko;
    char plec;
    List<Double> ocenyWIndeksie;

    @Override
    public String toString() {
        return "Student{" +
                "nrIndeksu=" + nrIndeksu +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", plec=" + plec +
                '}';
    }

    protected double obliczSredniaStudenta(Student student){
        double srednia=0;
        for (Double d: student.ocenyWIndeksie) {
            srednia += d;
        }
        srednia = srednia/student.ocenyWIndeksie.size();

        return srednia;
    }

    protected boolean czyStudentZdal(){
        for (Double o: ocenyWIndeksie) {
            if(o<=2){
                return false;
            }
        }
        return true;
    }
}
