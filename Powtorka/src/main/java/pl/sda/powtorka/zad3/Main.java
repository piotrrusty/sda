package pl.sda.powtorka.zad3;

public class Main {
    public static void main(String[] args) {
        CentrumKonferencyjne centrum = new CentrumKonferencyjne();

        SalaKonferencyjna s1 = new SalaKonferencyjna(1, Stan.WOLNA, 20, 300);
        SalaKonferencyjna s2 = new SalaKonferencyjna(2, Stan.WOLNA, 15, 250);
        SalaKonferencyjna s3 = new SalaKonferencyjna(3, Stan.WOLNA, 26, 450);
        SalaKonferencyjna s4 = new SalaKonferencyjna(4, Stan.WOLNA, 10, 200);
        SalaKonferencyjna s5 = new SalaKonferencyjna(5, Stan.WOLNA, 50, 1000);


        centrum.dodajSaleKonferencyjna(s1);
        centrum.dodajSaleKonferencyjna(s2);
        centrum.dodajSaleKonferencyjna(s3);
        centrum.dodajSaleKonferencyjna(s4);
        centrum.dodajSaleKonferencyjna(s5);
//        System.out.println(centrum.dodajSaleKonferencyjna(s3));

//        System.out.println(centrum.listaSal);

//        centrum.wylistujWszytskieSale();

        System.out.println(centrum.znajdzNajtanszaSale(10));


    }


}