package pl.sda.powtorka.zad3;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class CentrumKonferencyjne {

    Stan stanCentrum;
    List<SalaKonferencyjna> listaSal = new ArrayList<>();


    boolean dodajSaleKonferencyjna(SalaKonferencyjna sala) {
        for (SalaKonferencyjna s : listaSal) {
            if (s.getNrSali() == sala.getNrSali()) {
                return false;
            }
        }
        this.listaSal.add(sala);
        return true;
    }

    void wylistujWszytskieSale() {
        for (SalaKonferencyjna s : listaSal) {
            System.out.println(s);
        }
    }

    Optional<SalaKonferencyjna> znajdzNajtanszaSale(int iloscPotrzebnychMiejsc) {
//        SalaKonferencyjna odpowiedniaSala;
//        List<SalaKonferencyjna> sale = new ArrayList<>();
//        for (SalaKonferencyjna s : listaSal) {
//            if (s.getPojemnoscSali() >= iloscPotrzebnychMiejsc) {
//                sale.add(s);
//            }
//        }
Optional<SalaKonferencyjna> szukana = listaSal.stream()
        .filter(SalaKonferencyjna -> (SalaKonferencyjna.getStanSali() == Stan.WOLNA))
        .filter(SalaKonferencyjna -> (SalaKonferencyjna.getPojemnoscSali() >= iloscPotrzebnychMiejsc))
        .sorted(((o1, o2) -> Integer.compare(o1.getCenaSali(), o2.getCenaSali())))
        .findFirst();

//        SalaKonferencyjna.KomparatorSal komparator = new SalaKonferencyjna.KomparatorSal();
//        Collections.sort(sale, komparator);
//        odpowiedniaSala = sale.get(0);

        return szukana;

    }
}
