package pl.sda.powtorka.zad3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@AllArgsConstructor
@Getter
@Setter
public class SalaKonferencyjna {
    int nrSali;
    Stan stanSali;
    int pojemnoscSali;
    int cenaSali;

    @Override
    public String toString() {
        return "SalaKonferencyjna{" +
                "nrSali=" + nrSali +
                ", stanSali=" + stanSali +
                ", pojemnoscSali=" + pojemnoscSali +
                ", cenaSali=" + cenaSali +
                '}';
    }

    public static class KomparatorSal implements Comparator<SalaKonferencyjna>{

        @Override
        public int compare(SalaKonferencyjna o1, SalaKonferencyjna o2) {
            if(o1.getCenaSali()>o2.getCenaSali()){
                return 1;
            }
            else if(o1.getCenaSali()<o2.getCenaSali()){
                return -1;
            }
            return 0;
        }
    }
}
