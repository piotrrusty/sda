package pl.sda.generyczne.pudelko;

public class Pudelko<T> {

    private T costam;

    public Pudelko(T costam) {
        this.costam = costam;
    }

    public void setCostam(T costam) {
        this.costam = costam;
    }

    public T getCostam() {

        return costam;
    }

    public boolean czyPudelkoJestPuste(){
        if(costam==null){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Pudelko<String> pudelko = new Pudelko<>("cos tam");
        Pudelko<String> pudelko2 = new Pudelko<>(null);

        System.out.println(pudelko.czyPudelkoJestPuste());
        System.out.println(pudelko2.czyPudelkoJestPuste());
    }
}
