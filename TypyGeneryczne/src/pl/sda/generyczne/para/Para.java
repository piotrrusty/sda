package pl.sda.generyczne.para;


public class Para<P, L> {
    private P prawa;
    private L lewa;

    public Para(P prawa, L lewa) {
        this.prawa = prawa;
        this.lewa = lewa;
    }


    public P getPrawa() {
        return prawa;
    }

    public L getLewa() {
        return lewa;
    }

    public static Para[] znajdzNiepuste(Para[] pary) {
        int licznik = 0;
        for (Para para : pary) {
            if (para.getLewa() != null &&
                    para.getPrawa() != null) {
                //
                licznik++;
            }
        }
        Para[] tablicaWynikowa = new Para[licznik]; // licznik =3
        int indeksWstawiania = 0;
        for (int i = 0; i < pary.length; i++) {
            Para para = pary[i];
            if (para.getLewa() != null &&
                    para.getPrawa() != null) {
                //
                tablicaWynikowa[indeksWstawiania] = para;
                indeksWstawiania++;
            }
        }
        return tablicaWynikowa;
    }
}
//    public static void main(String[] args) {
//
//
//    Para<String, String> para = new Para<>("Marian", "Helena");
//    Para<String, String> para2 = new Para<>("Janusz", "Janina");
//    Para<String, String> para3 = new Para<>("Ryszard", "Krystyna");
//
//    Para[] pary = new Para[];
//        pary[0] = para;
//        pary[1] = para2;
//        pary[2] = para3;
//    }

