package pl.sda.generyczne.tostring;

public class ToString {

    public static void main(String[] args) {
        ToString string = new ToString();

        wypisz10Razy(5);
        wypiszXRazy(5,67,3,456,32,343,556,56);
    }

    public static <T> void wypisz10Razy (T obiekt){
        for(int i = 0; i<10; i++){

            System.out.println(obiekt.toString());
        }
    }

    public static <T> void wypiszXRazy (T... obiekt){
        for (int i = 0; i < obiekt.length; i++) {
            System.out.println(obiekt[i].toString());
        }
    }
}
