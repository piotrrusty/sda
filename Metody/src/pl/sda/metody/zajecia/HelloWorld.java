package pl.sda.metody.zajecia;

public class HelloWorld {
    public static void main(String[] args) {
        hello();
        hello("Piotrek");
        hello("Ania");
    }

    private static void hello(String imie) {
        System.out.println("Cześć, mam na imię "+imie);
    }

    static void hello () {
        System.out.println("Hello World!");
    }
}
