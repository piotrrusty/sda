package pl.sda.metody.zajecia;

public class Imiona {
    public static void main(String[] args) {
        String[] imiona = new String[] {"Ania", "Piotrek", "Kuba", "Zbigniew", "Wieslaw", "Gabrysia", "Andrzej", "Tomasz", "Urszula", "Arkadiusz"};

        System.out.println("Wszystkie imiona:");
        for (String imie:imiona) {
            System.out.println(imie);
        }

        System.out.println("Tylko imiona żeńskie");
        for (String imie:imiona) {
            if (czyImieZenskie(imie)) {
                System.out.println(imie);
            }
        }

        System.out.println("Tylko imiona męskie");
        for (String imie:imiona) {
            if (!czyImieZenskie(imie)) {
                System.out.println(imie);
            }
        }
    }

    private static boolean czyImieZenskie(String imie) {
        if (imie.endsWith("a") && !imie.equals("Kuba")){
            return true;
        }else {
            return false;
        }
    }


}
