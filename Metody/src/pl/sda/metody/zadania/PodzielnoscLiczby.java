package pl.sda.metody.zadania;

import java.util.Scanner;

public class PodzielnoscLiczby {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Wybierz liczbe, ktora zbadam");

        int liczba = input.nextInt();
        System.out.println("Wybierz dzielnik");
        int dzielnik = input.nextInt();

        if (czyLiczbaPodzielna(liczba, dzielnik)){
            System.out.println(String.format("Liczba %d jest podzielna przez %d", liczba, dzielnik));
        }else{
            System.out.println(String.format("Liczba %d nie jest podzielna przez %d", liczba, dzielnik));
        }


//        Scanner input = new Scanner(System.in);
//        System.out.println("Podaj liczbę");
//
//        int liczba = input.nextInt();
//        drukujWynik(liczba);
//    }
//
//    private static boolean podzielnoscPrzez3 (int liczba) {
//        return liczba%3 ==0 || liczba%5 ==0;
//    }
//
//    private static boolean podzielnoscPrzez5 (int liczba) {
//        return liczba%5 == 0;
//    }
//
//    private static void drukujWynik (int liczba) {
//        if (podzielnoscPrzez3(liczba)) {
//            System.out.println("Liczba jest podzielna przez 3");
//            if (podzielnoscPrzez5(liczba)) {
//                System.out.println("Liczba jest podzielna przez 5");
//            }
//        }else{
//            System.out.println("Liczba nie jest podzielna przez 3");
//        }
//    }
}

    private static boolean czyLiczbaPodzielna(int liczba, int dzielnik) {
        return liczba%dzielnik==0;
        }
    }

