package pl.sda.metody.zadania;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj rodzaj działania: *,+,-,/,%");
        String dzialanie = input.nextLine();
        System.out.println("Podaj pierwszą liczbę");
        int pierwsza = input.nextInt();
        System.out.println("Podaj drugą liczbę");
        int druga = input.nextInt();

        switch (dzialanie) {
            case "*":
                mnozenie(pierwsza, druga);
                break;
            case "/":
                dzielenie(pierwsza, druga);
                break;
            case "+":
                dodawanie(pierwsza, druga);
                break;
            case "-":
                odejmowanie(pierwsza, druga);
                break;
            case "%":
                modulo(pierwsza, druga);

        }

    }

    private static void odejmowanie(int pierwsza, int druga) {
        double wynik = pierwsza - druga;
        System.out.println(String.format("Wynikiem odejmowania %d od %d jest %.3f", druga, pierwsza, wynik));
    }

    private static void dodawanie(int pierwsza, int druga) {
        double wynik = pierwsza + druga;
        System.out.println(String.format("Wynikiem dodawania %d i %d jest %.3f", pierwsza, druga, wynik));
    }

    private static void mnozenie(int pierwsza, int druga) {
        double wynik = pierwsza * druga;
        System.out.println(String.format("Wynikiem mnożenia %d i %d jest %.0f", pierwsza, druga, wynik));
    }

    private static void dzielenie(int pierwsza, int druga) {
        double wynik = pierwsza / (double) druga;
        System.out.println(String.format("Wynikiem dzielenia %d przez %d jest %f", pierwsza, druga, wynik));
    }

    private static void modulo(int pierwsza, int druga) {
        String dzialanie = "%";
        int wynik = pierwsza % druga;
        // System.out.println(String.format("Resztą z dzielenia %d przez %d jest %d", pierwsza, druga, wynik));
        System.out.println(pierwsza + " " + dzialanie + " " + druga + " = " + wynik);
    }

}
