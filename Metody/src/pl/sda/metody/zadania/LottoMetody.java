package pl.sda.metody.zadania;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class LottoMetody {
    public static void main(String[] args) {
        int[] wytypowane = getNumbersFromUser();
        Arrays.sort(wytypowane);
        System.out.println("Wytypowałeś liczby: " + Arrays.toString(wytypowane));

        int[] wylosowane = new int[6];

        for (int i = 0; i < 6; i++) {
            int liczba = losowanie();
            wylosowane[i] = liczba;
        }

        Arrays.sort(wylosowane);

        System.out.println("Wylosowano liczby: " + Arrays.toString(wylosowane));

        System.out.println("Trafiłeś liczb: " + porownajWyniki(wytypowane, wylosowane));


    }


    public static int[] getNumbersFromUser() {
        int[] wytypowane = new int[6];
        int wytypowanaLiczba;

        for (int i = 0; i < 6; i++) {
            Scanner input = new Scanner(System.in);
            do {
                System.out.println("Podaj liczbę nr " + (i + 1) + ". Liczby muszą być z zakresu 1-49 i nie mogą się powtarzać.");
                wytypowanaLiczba = input.nextInt();
                if (wytypowanaLiczba > 49) {
                    System.out.println("Podano zbyt dużą liczbę!");
                }
            } while ((isUnique(wytypowanaLiczba, wytypowane)) || wytypowanaLiczba > 49);
            wytypowane[i] = wytypowanaLiczba;

        }
        return wytypowane;
    }

    private static int losowanie() {
        Random r = new Random();
        int[] tabelaLosowanych = new int[6];
        int wylosowanaLiczba;
        do {
            wylosowanaLiczba = r.nextInt(49) + 1;
        } while (isUnique(wylosowanaLiczba, tabelaLosowanych));
        return wylosowanaLiczba;
    }

    private static boolean isUnique(int liczba, int[] tablica) {
        for (int i = 0; i < 6; i++) {
            if (tablica[i] == liczba) return true;
        }
        return false;
    }

    private static int porownajWyniki(int[] tabelaWytypowanych, int[] tabelaWylosowanych) {

        int trafione = 0;
        for (int i = 0; i < tabelaWylosowanych.length; i++) {
            for (int j = 0; j < tabelaWytypowanych.length; j++) {
                if (tabelaWylosowanych[i] == tabelaWytypowanych[j]) {
                    trafione++;
                }
            }
        }return trafione;

    }
}
