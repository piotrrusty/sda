package pl.sda.metody.zadania;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Lotto {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("WITAMY W LOTTO!");
        System.out.println("Wytypuj szcześć liczb i sprawdź wyniki!");

        int[] typowane = new int[6];

        for (int i = 0; i <= 5; i++) {
            System.out.println("Podaj liczbę nr " + (i + 1));
            typowane[i] = input.nextInt();
            if (typowane[i] > 49) {
                System.out.println("Podałeś za dużą liczbę!");
                i--;
            }
            if (typowane[i] < 1) {
                System.out.println("Podałeś za małą liczbę!");
                i--;
            }
        }
        Arrays.sort(typowane);
        System.out.println("Wytypowałeś liczby: " + Arrays.toString(typowane));
        System.out.println();
        System.out.println("Następuje losowanie 6 liczb");

        Random random = new Random();

        int[] losowane = new int[6];

        for (int i = 0; i <= 5; i++) {
            losowane[i] = random.nextInt(49) + 1;
        }
        Arrays.sort(losowane);
        System.out.println("Wylosowano liczby " + Arrays.toString(losowane));
    }
}
