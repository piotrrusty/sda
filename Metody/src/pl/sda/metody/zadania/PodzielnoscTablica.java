package pl.sda.metody.zadania;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ppomorsk on 02/03/2018.
 * Treść Zadania:
 * Napisz program, który przyjmuje tablicę dzielników oraz liczbę całkowitą, a następnie zwraca
 * wszystkie liczby mniejsze od zadanej liczby całkowitej podzielne przez elementy z tablicy dzielników np.
 */
public class PodzielnoscTablica {

    public static void main(String[] args) {

        System.out.print("Podaj iluelementowa ma być tablica: ");
        Scanner scanner = new Scanner(System.in);
        int rozmiar = scanner.nextInt();

        int[] tablica = new int[rozmiar];

        System.out.println("Podaj elementy tablicy");
        for (int i = 0; i < rozmiar; i++) {
            System.out.print((i + 1) + " liczba - ");
            tablica[i] = scanner.nextInt();
        }
        System.out.println("\nMoja tablica dzielników: ");
        System.out.println(Arrays.toString(tablica));

        System.out.println("Podaj dzielną");
        int dzielna = scanner.nextInt();

        sprawdzPodzielnosc(tablica, dzielna);
    }

    private static void sprawdzPodzielnosc(int[] tablicaWartosci, int dzielna) {
        System.out.println("Dla liczby " + dzielna + " i dzielników " + Arrays.toString(tablicaWartosci) + " znaleziono:");
        for (int dzielnik : tablicaWartosci) {
            zwrocWszystkieLiczbyPodzielnePrzezIMniejszeOd(dzielnik, dzielna);
        }
    }

    private static int[] zwrocWszystkieLiczbyPodzielnePrzezIMniejszeOd(int dzielnik, int liczba) {


        int wystapienia = 0;
        for (int i = 1; i < liczba; i++) {
            if (i % dzielnik == 0) {
                wystapienia++;
            }
        }
        int[] tablica = new int[wystapienia];

        int elementTablicy = 0;
        for (int j = 1; j < liczba; j++) {
            if (j % dzielnik == 0) {
                tablica[elementTablicy] = j;
                elementTablicy++;
            }
        }

        if(dzielnik > liczba){
            System.out.println("Podany dzielnik " + dzielnik + " jest większy dzielnej.");
        }
        else if(tablica.length==0){
            System.out.println("Brak dzielników dla liczby " + dzielnik);
        } else {
            System.out.println("Liczby podzielne przez " + dzielnik + ": " + Arrays.toString(tablica));
        }
        return tablica;
    }


}