package pl.sda.metody.zadania;

public class DrukujParzysteLiczby {
    public static void main(String[] args) {

        drukujParzystoscLiczby(17);

    }

    private static void drukujParzystoscLiczby(int liczba){
        if (czyLiczbaJestParzysta(liczba)) {
            System.out.println("liczba jest parzysta");
        }else{
            System.out.println("liczba jest nieparzysta");
        }
    }

    private static boolean czyLiczbaJestParzysta (int liczba){
        if (liczba % 2 == 0) {
            return true;
        }else{
            return false;
        }
    }
}
