package pl.sda.zadania.weekend;

import java.util.Scanner;

public class PetlaDoWhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int liczba = input.nextInt();
        int liczba2=1;

        if (liczba > 0){
            do {
                System.out.println(liczba2);
                liczba2++;
            }while(liczba2<liczba);
        }
    }
}
