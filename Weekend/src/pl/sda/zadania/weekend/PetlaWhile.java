package pl.sda.zadania.weekend;

import java.util.Scanner;

public class PetlaWhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbę do obliczeń");
        int liczba = input.nextInt();
        int licznik=0;
        int wynik=0;
        while(licznik <= liczba){
            wynik += licznik;
            licznik++;
        }

        System.out.println(wynik);
    }
}
