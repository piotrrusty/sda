package pl.sda.zadania.weekend;

import java.util.Arrays;
import java.util.Scanner;

public class StatystykiTablicy {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj jak duża ma być tablica");
        int wielkosc = input.nextInt();

        int[] tablicaAAA = new int[wielkosc];

        for (int i = 0; i < tablicaAAA.length; i++) {
            System.out.println("Podaj element tablicy nr " + (i + 1));
            tablicaAAA[i] = input.nextInt();
        }
        drukujOpisTablicy(tablicaAAA);

//        System.out.println();
//
//        System.out.println("Czy chcesz kontynuować? Wpisz 'tak' lub 'nie'.");
//        String wybor = input.next();
//        if(wybor.equals("tak")){
//            drukujOpisTablicy(tablicaAAA);
//        }
    }

    private static void drukujOpisTablicy(int[] tablica) {

        Scanner input = new Scanner(System.in);

        // znajdowanie wartosci minimalnej
        int wartoscMin = tablica[0];
        for (int wartosc : tablica) {
            if (wartosc < wartoscMin) {
                wartoscMin = wartosc;
            }
        }

        // znajdowanie wartosci maxymalnej
        int wartoscMax = tablica[0];
        for (int wartosc : tablica) {
            if (wartosc > wartoscMax) {
                wartoscMax = wartosc;
            }
        }

        // znajdowanie sredniej
        int suma = 0;
        for (int wartosc : tablica) {
            suma += wartosc;
        }

        double srednia = (double) suma / tablica.length;
        System.out.println("Wartosc minimalna to:" + wartoscMin);
        System.out.println("Wartość maxymalna to: " + wartoscMax);
        System.out.println("Suma elementow tablicy to: " + suma);
        System.out.println("Srednia elementow tablicy to: " + srednia);
        System.out.println("Tablica składa się z " + tablica.length + " elementów.");

        System.out.println();
        String wybor;
        do {

            System.out.println("Czy chcesz kontynuować? Wpisz 'tak' lub 'nie'.");
            wybor = input.next();


            if (!wybor.equalsIgnoreCase("tak") || !wybor.equalsIgnoreCase("nie")) {
                System.out.println("Podałeś złą komendę. Wpisz 'tak' lub 'nie'");
            }
            if (wybor.equalsIgnoreCase("tak")) {
                drukujOpisTablicy(tablica);
            }
            if (wybor.equalsIgnoreCase("nie")) {
                break;
            }
        } while (!wybor.equalsIgnoreCase("nie"));

    }
}
