package pl.sda.zadania.weekend;

import java.util.Scanner;

public class PetlaDoWhilePierwiastek {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj liczbę, której pierwiastek obliczę");
        int liczba = input.nextInt();
        if (liczba > 0) {

            double pierwiastek = Math.sqrt(liczba);
            System.out.println(pierwiastek);
        }
    }
}
