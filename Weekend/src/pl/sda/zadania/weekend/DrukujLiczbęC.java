package pl.sda.zadania.weekend;

import java.util.Random;
import java.util.Scanner;

public class DrukujLiczbęC {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Podaj minnimalną liczbę");
        int liczbaMin = input.nextInt();
        System.out.println("Podaj maksymalną liczbę");
        int liczbaMax = input.nextInt();

        int wylosowana;
        do {
            wylosowana = random.nextInt(liczbaMax-liczbaMin+1)+(liczbaMin);

        } while (wylosowana < liczbaMin);

        System.out.println(wylosowana);

    }
}
