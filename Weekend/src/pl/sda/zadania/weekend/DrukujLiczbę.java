package pl.sda.zadania.weekend;

import java.util.Random;
import java.util.Scanner;

public class DrukujLiczbę {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Podaj liczbę");
        int liczba = input.nextInt();

        int wylosowana = random.nextInt(liczba+1);

        System.out.println(wylosowana);
    }
}
