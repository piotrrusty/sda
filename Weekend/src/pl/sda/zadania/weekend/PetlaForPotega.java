package pl.sda.zadania.weekend;

import java.util.Scanner;

public class PetlaForPotega {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbę, którą podniosę do wyznaczonej potęgi");
        int n = input.nextInt();
        System.out.println("Podaj potęgę");
        int m = input.nextInt();
        int wynik = 1;


        for (int i = 0; i < m; i++) {
            wynik *= n;
        }
            System.out.println(wynik);


    }
}
