package pl.sda.zadania.weekend;

import java.util.Scanner;

public class PetlaWhileSilnia {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj liczbę, MAX: 12");

        int liczba = input.nextInt();
        int licznik = 1;
        int wynik = 1;

        if (liczba <= 12) {
            while (licznik <= liczba) {
                wynik *= licznik;
                licznik++;
            }

            System.out.println(wynik);
        }
    }
}
