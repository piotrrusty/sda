package pl.sda.zadania.weekend2.samochody;

public class Samochod {

    int predkosc=0;
    int rocznik;
    String kolor;
    String marka;

    public Samochod (String marka, String kolor, int rocznik) {
        this.marka = marka;
        this.kolor = kolor;
        this.rocznik = rocznik;
    }

    protected void przyspiesz(){
            this.predkosc += 20;
        if(this.predkosc<140){
        }else{
            System.out.println("Osiągnięto max prędkość");
        }
    }

    @Override
    public String toString() {
        return String.format("Samochód %s, kolor: %s, rocznik %d", marka, kolor, rocznik);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Samochod){
            Samochod that = (Samochod) obj;
            if (this.kolor==that.kolor && this.marka==that.marka && this.rocznik==that.rocznik){
                return true;
            }

        }
        return false;
    }
}
