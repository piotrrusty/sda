package pl.sda.zadania.weekend2.samochody;

public class SzybkiSamochod extends Samochod {
    public SzybkiSamochod(String marka, String kolor, int rocznik) {
        super(marka, kolor, rocznik);
    }

    @Override
    protected void przyspiesz() {
        this.predkosc += 20;
        if (this.predkosc < 200) {
        } else {
            System.out.println("Osiągnięto max prędkość");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SzybkiSamochod) {
            Samochod that = (SzybkiSamochod) obj;
            if (this.kolor == that.kolor && this.marka == that.marka && this.rocznik == that.rocznik) {
                return true;
            }
        }
        return false;
    }
}
