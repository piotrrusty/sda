package pl.sda.zadania.weekend2.samochody;

public class Main {
    public static void main(String[] args) {
        Samochod sam1 = new Samochod("BMW", "czarny", 2005);
        Samochod sam2 = new Samochod("Audi", "niebieski", 2011);
        Samochod sam3 = new Samochod("Audi", "niebieski", 2011);
        SzybkiSamochod sam4 = new SzybkiSamochod("BMW", "czarny", 2005);

        System.out.println(sam1);

        System.out.println(sam1.equals(sam2));
        System.out.println(sam2.equals(sam3));
        System.out.println(sam4.equals(sam1));
        System.out.println(sam1.equals(sam4));
    }
}
