package pl.sda.zadania.weekend2;

public enum GatunekPiwa {
    LAGER,
    PILZNER,
    STOUT,
    PORTER,
    MIODOWE;


    public void wyswietlDanePiwa() {

        String nazwa = this.toString();
        System.out.println("Piwo " + nazwa);

    }


//    public GatunekPiwa[] opisPiwa() {
//        System.out.println(String.valueOf(this));
//        return GatunekPiwa[]
//    }
}
