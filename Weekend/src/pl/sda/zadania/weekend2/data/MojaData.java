package pl.sda.zadania.weekend2.data;

public class MojaData {
    int dzien;
    int miesiac;
    int rok;


    public MojaData(int dzien, int miesiac, int rok) {
        this.dzien = dzien;
        this.miesiac = miesiac;
        this.rok = rok;

    }

    public void wyswietlDate() {

        String dzienS;
        String miesiacS;

        if (dzien < 10 && miesiac > 9) {
            dzienS = "0" + String.valueOf(dzien);
            System.out.printf("%s.%d.%d", dzienS, miesiac, rok);
        }
        if (dzien < 10 && miesiac < 10) {
            dzienS = "0" + String.valueOf(dzien);
            miesiacS = "0" + String.valueOf(miesiac);
            System.out.printf("%s.%s.%d", dzienS, miesiacS, rok);
        }
        if (dzien > 9 && miesiac < 10) {
            miesiacS = "0" + String.valueOf(miesiac);
            System.out.printf("%d.%s.%d", dzien, miesiacS, rok);
        }
        if (dzien > 9 && miesiac > 9) {
            System.out.printf("%d.%d.%d", dzien, miesiac, rok);
        }
    }


        public static void main (String[]args){
            MojaData data = new MojaData(5, 2, 1989);

            data.wyswietlDate();
        }
    }

