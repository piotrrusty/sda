package com.sda.dijkstra;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Graf {
    Map<Integer, Wierzcholek> wierzcholki = new HashMap<>();
    private PriorityQueue<Wierzcholek> kolejka;

    public Graf() {

        kolejka = new PriorityQueue<>(new Comparator<Wierzcholek>() {
            @Override
            public int compare(Wierzcholek o1, Wierzcholek o2) {
                if (o1.getDystans() > o2.getDystans()) {
                    return -1;
                } else if (o1.getDystans() < o2.getDystans()) {
                    return 1;
                }
                return 0;
            }
        });

    }

    public void dodajWierzcholek(Wierzcholek w){
        wierzcholki.put(w.getIdentyfikator(), w);
    }

    public void dijkstra(int start, int stop) {
        Wierzcholek wierzcholekStart = wierzcholki.get(start);
        wierzcholekStart.setDystans(0.0);
        wierzcholekStart.setTrasa(wierzcholekStart.getNazwa());

        kolejka.add(wierzcholekStart);
        while(!kolejka.isEmpty()){

            Wierzcholek wierzcholek = kolejka.poll();

            if(wierzcholek.getIdentyfikator() == stop){
                break;
            }

            for (int i =0; i < wierzcholek.getSasiedzi().size(); i++){
                int idSasiada = wierzcholek.getSasiedzi().get(i);
                Krawedz krawedzSasiada = wierzcholek.getKrawedzie().get(i);

                double alternatywnyDystansDoSasiada = wierzcholek.getDystans() + krawedzSasiada.getWaga();
                Wierzcholek sasiad = wierzcholki.get(idSasiada);

                if(alternatywnyDystansDoSasiada< sasiad.getDystans()){
                    sasiad.setDystans(alternatywnyDystansDoSasiada);
                    kolejka.add(sasiad);
                }
            }

        }
        Wierzcholek koncowy = wierzcholki.get(stop);

        System.out.println("Dystans (do koncowego wierzcholka): " + koncowy.getDystans());
    }
}
