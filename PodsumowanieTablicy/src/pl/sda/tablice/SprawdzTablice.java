package pl.sda.tablice;

import java.util.Arrays;
import java.util.Scanner;

public class SprawdzTablice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj iloelementowa ma być tablica");
        int wielkosc = scanner.nextInt();
        int[] tablica = new int[wielkosc];

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Podaj liczbe #" + i);
            tablica[i] = scanner.nextInt();
        }

        Arrays.sort(tablica);

        System.out.println(Arrays.toString(tablica));

        int max = tablica[0];
        for (int liczba : tablica) {
            if (liczba > max) {
                max = liczba;
            }
        }

        int min = tablica[0];
        for (int liczba : tablica) {
            if (liczba < min) {
                min = liczba;
            }
        }

        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];

        }

        double srednia = suma / (double) wielkosc;

        Arrays.sort(tablica);

        System.out.println("wielkość tablicy to " + tablica.length);

        double mediana = 0.0;
        if (tablica.length % 2 == 1) {
            Arrays.sort(tablica);
            mediana = tablica[(tablica.length - 1) / 2];
        }
        if (tablica.length % 2 == 0) {
            Arrays.sort(tablica);
            mediana = (double)(tablica[(tablica.length / 2) - 1] + tablica[(tablica.length / 2)]) / 2;
        }


        System.out.println("");

        System.out.println("Podsumowanie tablicy:");
        System.out.println("Maksymalna wartosc " + max);
        System.out.println("Minimalna wartość " + min);
        System.out.println("Suma wartości elementów tablicy to " + suma);
        System.out.println("Srednia wartosc elentow tablicy to " + srednia);
        System.out.println("Mediana elementów tablicy to " + mediana);
    }
}
