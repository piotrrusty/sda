package pl.sda.osoby;

import java.util.ArrayList;
import java.util.List;

public class Programmer {
    Person person;
    List<String> listaJezykow = new ArrayList<>();

    public Programmer(Person person, List<String> listaJezykow) {
        this.person = person;
        this.listaJezykow = listaJezykow;
    }

    @Override
    public String toString() {
        return "Programmer{" +
                "person=" + person +
                '}';
    }

    public Person getPerson() {
        return person;
    }

    public List<String> getListaJezykow() {
        return listaJezykow;
    }
}
