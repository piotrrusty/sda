package pl.sda.osoby;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    static List<Person> people = new ArrayList<>();

    public static void main(String[] args) {

        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");

        Programmer programmer1 = new Programmer(person1,languages1);
        Programmer programmer2 = new Programmer(person2,languages2);
        Programmer programmer3 = new Programmer(person3,languages3);
        Programmer programmer4 = new Programmer(person4,languages4);
        Programmer programmer5 = new Programmer(person5,languages5);
        Programmer programmer6 = new Programmer(person6,languages6);
        Programmer programmer7 = new Programmer(person7,languages7);
        Programmer programmer8 = new Programmer(person8,languages8);
        Programmer programmer9 = new Programmer(person9,languages9);

        List<Programmer> programmers = Arrays.asList(programmer1,programmer2,programmer3,programmer4,
                programmer5,programmer6,programmer7,programmer8,programmer9);
        System.out.println(programmers);




        people = (Arrays.asList(person1, person2, person3, person4, person5, person6, person7, person8, person9));

        System.out.println("Mezczyzni " + pokazMezczyzn(people));
        System.out.println("Dorosłe kobiety " + pokazDorosleKobiety(people));
        System.out.println("Dorosły Jacek " + pokazDoroslegoJacka(people));
        System.out.println("Osoby od 15 do 19 lat: " + pokazNazwiskaOsobWWieku15Do19(people));
        System.out.println("Suma wieku wszystkich osob to: " + uzyskajSumeWieku(people));
        System.out.println("Srednia wieku mezczyzn to " + uzyskajSredniaWiekuMezczyzn(people));
        System.out.println("Najstarsza osoba na liscie to: "+ znajdzNajstarszaOsoba(people));

        System.out.println("Mezczyzni programisci to: " + znajdzMezczyznProgramistow(programmers));
        System.out.println("Niepelnoletni piszacy w Cobolu to: "+ znajdzNiepelnoletnichPiszacychWCobolu(programmers));
        System.out.println("Znajacy wiecej niz jeden jezyk to: "+ znajdzZnajacychWiecejNizJedenJezyk(programmers));
        System.out.println("Programistki piszace w Javie i Cpp to: " + znajdzKobietyPiszaceWJavieICpp(programmers));
        System.out.println("Imiona męskie to: "+ uzyskajListeMeskichImion(programmers));
//        System.out.println("Wszystkie języki znane przez progrmistów, to: " +uzyskajSetWszystkichJęzyków(programmers));


    }

    public static List<Person> pokazMezczyzn(List<Person> lista) {
        return people.stream()
                .filter(Person -> (Person.isMale() == true))
                .collect(Collectors.toList());
    }

    public static List<Person> pokazDorosleKobiety(List<Person> lista) {
        return people.stream()
                .filter(Person -> (Person.isMale() == false && Person.getAge() >= 18))
                .collect(Collectors.toList());
    }

    public static Optional<Person> pokazDoroslegoJacka(List<Person> lista) {
        Optional<Person> jacek = people.stream()
                .filter(Person -> (Person.getName().toLowerCase().equals("jacek") && Person.getAge() >= 18))
                .findAny();
        return jacek;

    }

    public static List<String> pokazNazwiskaOsobWWieku15Do19(List<Person> lista) {
        return people.stream()
                .filter(Person -> (Person.getAge() >= 15 && Person.getAge() < 20))
                .map(person -> person.getSurname())
                .collect(Collectors.toList());
    }

    public static int uzyskajSumeWieku(List<Person> lista) {
        int suma = lista.stream().mapToInt(Person::getAge).sum();
        return suma;
    }

    public static double uzyskajSredniaWiekuMezczyzn(List<Person> lista) {
        int licznik=0;
        int sumaWiekuMezczyzn = lista.stream()
                .filter(Person -> (Person.isMale() == true))
                .mapToInt(Person::getAge).sum();
        int liczbaMezczyzn = (int) lista.stream()
                .filter(Person -> (Person.isMale() == true))
                .count();
        double srednia = (double) sumaWiekuMezczyzn/liczbaMezczyzn;

        return srednia;
    }

    public static Optional<Person> znajdzNajstarszaOsoba (List<Person> lista){
        return lista.stream()
                .sorted((o2, o1) -> Integer.compare(o1.getAge(), o2.getAge()))
                .findFirst();
    }

    public static List<Programmer> znajdzMezczyznProgramistow (List<Programmer> lista){
        return lista.stream()
                .filter(Programmer -> (Programmer.person.isMale()))
                .collect(Collectors.toList());
    }

    public static List<Programmer> znajdzNiepelnoletnichPiszacychWCobolu(List<Programmer> lista){
        return lista.stream()
                .filter(Programmer -> (Programmer.listaJezykow.contains("Cobol")))
                .filter(Programmer -> (Programmer.person.getAge()<18))
                .collect(Collectors.toList());
    }

    public static List<Programmer> znajdzZnajacychWiecejNizJedenJezyk (List<Programmer> lista){
        return lista.stream()
                .filter(Programmer -> (Programmer.listaJezykow.size()>1))
                .collect(Collectors.toList());
    }

    public static List<Programmer> znajdzKobietyPiszaceWJavieICpp(List<Programmer> lista){
        return lista.stream()
                .filter(Programmer -> (!Programmer.person.isMale()))
                .filter(Programmer -> (Programmer.listaJezykow.contains("Java") /*&& Programmer.listaJezykow.contains("Cpp")*/  && Programmer.listaJezykow.toString().toLowerCase().contains("cpp")))
                .collect(Collectors.toList());
    }

    public static List<String> uzyskajListeMeskichImion (List<Programmer> lista){
        return lista.stream()
                .filter(Programmer -> (Programmer.person.isMale()))
                .map(Programmer -> Programmer.person.getName())
                .collect(Collectors.toList());
    }

//    public static Set<String> uzyskajSetWszystkichJęzyków (List<Programmer> lista){
//        return lista.stream()
//                .map(Programmer -> Programmer.getListaJezykow())
//                .collect(Collectors.toSet());
//    }
}
