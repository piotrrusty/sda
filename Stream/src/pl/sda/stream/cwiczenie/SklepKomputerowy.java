package pl.sda.stream.cwiczenie;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SklepKomputerowy {
    private List<Komputer> listaKomputerow = new ArrayList<>();

    public SklepKomputerowy(List<Komputer> list) {
        this.listaKomputerow.addAll(list);
    }

    public void wypiszWszystkieKomputery(){
        listaKomputerow.stream().forEach((komputer) -> {
            System.out.println(komputer);
        });
    }
    public void wypiszKomputeryOMocyWyzszejNiz(int moc){
        listaKomputerow.stream()
                .filter((komputer)-> {
                    return komputer.getPobieranaMoc() >= moc;
                })
                .forEach((komputer) -> {
                System.out.println(komputer);
        });

    }
    public void wypiszCenyNazwyIIdentyfikatoryWszystkichKomputerow(){
        listaKomputerow.stream().forEach((komputer) -> {
            System.out.println(komputer.cena+" "+komputer.nazwa+ " " + komputer.identyfikatorProduktu);
        });

    }
    public List<Komputer> zwróćWszystkieKomputery(){
        return listaKomputerow.stream().collect(Collectors.toList());
    }

    public void wypiszWszystkieKomputeryZDwomaProcesorami(){
        //List<Komputer> zDwoma = new ArrayList<>();
        listaKomputerow.stream()
                .filter((komputer)-> {
                    return komputer.getIloscProcesorow() >= 2;
                })
                .forEach((komputer) -> {
                    System.out.println(komputer);
                });


//        for (Komputer k: listaKomputerow) {
//            if(k.iloscProcesorow>=2){
//                System.out.println(k);
//            }
//        }

    }

    public List<Komputer> zwróćWszystkieKomputeryZDwomaProcesorami(){
        List<Komputer> zDwoma = listaKomputerow.stream()
                .filter((komputer)-> {
                    return komputer.getIloscProcesorow() == 2;
                })
                .collect(Collectors.toList());

        return zDwoma;

    }

    public List<Komputer> zwróćKomputeryZPamięciąORozmiarze (int rozmiarPamieci) {
        List<Komputer> zPamiecia = listaKomputerow.stream()
                .filter((komputer)-> {
                    return komputer.getWielkoscPamieciRAM() == rozmiarPamieci;
                })
                .collect(Collectors.toList());
        return zPamiecia;
    }

    public List<Komputer> zwróćTylkoKomputeryZTurboIProcesoremPowyzej4 (){
        List<Komputer> najlepsze = listaKomputerow.stream()
                .filter((komputer) -> {
                    return (komputer.czyProcesorMaTurbo && komputer.getIloscProcesorow()>=4);
                })
                .collect(Collectors.toList());
        return najlepsze;
    }

    public List<Komputer> zwróćKomputeryPosortowaniePoIdentyfikatorze() {
        List<Komputer> poIdentyfikatorze = listaKomputerow.stream()
                .sorted(new Comparator<Komputer>() {
                    @Override
                    public int compare(Komputer o1, Komputer o2) {
                        if (o1.getIdentyfikatorProduktu() > o2.getIdentyfikatorProduktu()){
                            return 1;
                        }else if (o1.getIdentyfikatorProduktu() < o2.getIdentyfikatorProduktu()){
                            return -1;
                        }
                        return 0;
                    }
                })
                .collect(Collectors.toList());
        return poIdentyfikatorze;
    }

    public List<Komputer> zwróćKomputeryPosortowaniePoCenieMalejąco() {
        List<Komputer> poCenieMalejaco = listaKomputerow.stream()
                .sorted(new Comparator<Komputer>() {
                    @Override
                    public int compare(Komputer o1, Komputer o2) {
                        if (o1.getCena() > o2.getCena()){
                            return -1;
                        }else if (o1.getCena() < o2.getCena()){
                            return 1;
                        }
                        return 0;
                    }
                })
                .collect(Collectors.toList());
        System.out.println(poCenieMalejaco);
        return poCenieMalejaco;
    }

    public List<Komputer> zwróćKomputeryPosortowaniePoCenieRosnąco() {
        List<Komputer> poCenieRosnaco = listaKomputerow.stream()
                .sorted(new Comparator<Komputer>() {
                    @Override
                    public int compare(Komputer o1, Komputer o2) {
                        if (o1.getCena() > o2.getCena()){
                            return 1;
                        }else if (o1.getCena() < o2.getCena()){
                            return -1;
                        }
                        return 0;
                    }
                })
                .collect(Collectors.toList());
        return poCenieRosnaco;
    }

    public Komputer zwróćNajbardziejOpłacalnyKomputer(){
        return listaKomputerow.stream()
                .sorted(new Comparator<Komputer>() {
                    @Override
                    public int compare(Komputer o1, Komputer o2) {
                        if (o1.getCena()/o1.getCzestotliwoscProcesora()<o2.getCena()/o2.getCzestotliwoscProcesora()){
                            return -1;
                        }else if(o1.getCena()/o1.getCzestotliwoscProcesora()>o2.getCena()/o2.getCzestotliwoscProcesora()){
                            return 1;
                        }
                        return 0;
                    }
                }).findFirst().get();
    }

    public Komputer zwróćNajmniejOpłacalnyKomputer(){
        return listaKomputerow.stream()
                .sorted(new Comparator<Komputer>() {
                    @Override
                    public int compare(Komputer o1, Komputer o2) {
                        if (o1.getCena()/o1.getCzestotliwoscProcesora()<o2.getCena()/o2.getCzestotliwoscProcesora()){
                            return 1;
                        }else if(o1.getCena()/o1.getCzestotliwoscProcesora()>o2.getCena()/o2.getCzestotliwoscProcesora()){
                            return -1;
                        }
                        return 0;
                    }
                }).findFirst().get();
    }

}
