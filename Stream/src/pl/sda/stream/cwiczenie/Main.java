package pl.sda.stream.cwiczenie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Komputer komputer1 = new Komputer(1, 2.3, true, 32.0, 512.0, 2300.0, 5, "Acer turbo", 300);
        Komputer komputer2 = new Komputer(2, 2.5, false, 16.0, 256.0, 1300.0, 3, "Acer not turbo", 350);
        Komputer komputer3 = new Komputer(5, 3.1, false, 8.0, 128.0, 1530.0, 9, "Acer 9 turbo", 1350);
        Komputer komputer4 = new Komputer(10, 1.1, true, 12.0, 60.0, 5130.0, 10, "Acer 10 turbo", 650);
        Komputer komputer5 = new Komputer(7, 4.1, true, 64.0, 30.0, 5000.0, 4, "Acer k4990 turbo", 500);
        Komputer komputer6 = new Komputer(3, 4.4, false, 30.0, 15.0, 4000.0, 3, "Acer 300 turbo", 200);

        List<Komputer> list = new ArrayList<>(Arrays.asList(komputer1, komputer2, komputer3, komputer4, komputer5, komputer6));

        SklepKomputerowy sklep = new SklepKomputerowy(list);

        sklep.zwróćKomputeryPosortowaniePoCenieMalejąco();
        sklep.zwróćKomputeryPosortowaniePoCenieRosnąco();
        System.out.println(sklep.zwróćNajbardziejOpłacalnyKomputer());
    }
}
