package pl.sda.stream.cwiczenie;

public class Komputer {
    int identyfikatorProduktu;
    double czestotliwoscProcesora;
    boolean czyProcesorMaTurbo;
    double wielkoscPamieciRAM;
    double wielkoscDysku;
    double cena;
    int iloscProcesorow;
    String nazwa;
    int pobieranaMoc;

    @Override
    public String toString() {
        return "Komputer{" +
                "identyfikatorProduktu=" + identyfikatorProduktu +
                ", czestotliwoscProcesora=" + czestotliwoscProcesora +
                ", czyProcesorMaTurbo=" + czyProcesorMaTurbo +
                ", wielkoscPamieciRAM=" + wielkoscPamieciRAM +
                ", wielkoscDysku=" + wielkoscDysku +
                ", cena=" + cena +
                ", iloscProcesorow=" + iloscProcesorow +
                ", nazwa='" + nazwa + '\'' +
                ", pobieranaMoc=" + pobieranaMoc +
                '}'+'\'';
    }

    public int getIdentyfikatorProduktu() {
        return identyfikatorProduktu;
    }

    public double getCzestotliwoscProcesora() {
        return czestotliwoscProcesora;
    }

    public boolean isCzyProcesorMaTurbo() {
        return czyProcesorMaTurbo;
    }

    public double getWielkoscPamieciRAM() {
        return wielkoscPamieciRAM;
    }

    public double getWielkoscDysku() {
        return wielkoscDysku;
    }

    public double getCena() {
        return cena;
    }

    public int getIloscProcesorow() {
        return iloscProcesorow;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getPobieranaMoc() {
        return pobieranaMoc;
    }


    public Komputer(int identyfikatorProduktu, double czestotliwoscProcesora, boolean czyProcesorMaTurbo, double wielkoscPamieciRAM, double wielkoscDysku, double cena, int iloscProcesorow, String nazwa, int pobieranaMoc) {
        this.identyfikatorProduktu = identyfikatorProduktu;
        this.czestotliwoscProcesora = czestotliwoscProcesora;
        this.czyProcesorMaTurbo = czyProcesorMaTurbo;
        this.wielkoscPamieciRAM = wielkoscPamieciRAM;
        this.wielkoscDysku = wielkoscDysku;
        this.cena = cena;
        this.iloscProcesorow = iloscProcesorow;
        this.nazwa = nazwa;
        this.pobieranaMoc = pobieranaMoc;
    }
}
