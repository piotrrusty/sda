package pl.sda.sklepsamochodowy;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CarShop {

    private String name;
    private List<CarOffer> carOffers;

    public CarShop(String name) {
        this.name = name;
        this.carOffers = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CarOffer> getCarOffers(){
        return carOffers;
    }

//    public Set<CarBrand> getCarBrand(){
//        return carOffers().stream()
//                .map(CarOffer -> carOffers.getCar().getMark())
//                .collect(Collectors.toSet());
//    }
}
