package pl.sda.komparator.zad3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DruzynaPilkarska {
    String nazwa;
    int pozycjaWRankingu;
    int silaDruzyny;

    public DruzynaPilkarska(String nazwa, int pozycjaWRankingu, int silaDruzyny) {
        this.nazwa = nazwa;
        this.pozycjaWRankingu = pozycjaWRankingu;
        this.silaDruzyny = silaDruzyny;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setPozycjaWRankingu(int pozycjaWRankingu) {
        this.pozycjaWRankingu = pozycjaWRankingu;
    }

    public void setSilaDruzyny(int silaDruzyny) {
        this.silaDruzyny = silaDruzyny;
    }

    public String getNazwa() {

        return nazwa;
    }

    public int getPozycjaWRankingu() {
        return pozycjaWRankingu;
    }

    public int getSilaDruzyny() {
        return silaDruzyny;
    }

    public static void main(String[] args) {
        DruzynaPilkarska dru1 = new DruzynaPilkarska("wilki", 1, 7);
        DruzynaPilkarska dru2 = new DruzynaPilkarska("owce", 7, 1);
        DruzynaPilkarska dru3 = new DruzynaPilkarska("kapcie", 10, 4);
        DruzynaPilkarska dru4 = new DruzynaPilkarska("sandały", 3, 6);
        DruzynaPilkarska dru5 = new DruzynaPilkarska("kalosze", 4, 2);
        DruzynaPilkarska dru6 = new DruzynaPilkarska("manchester", 5, 10);

        List<DruzynaPilkarska> ranking = new ArrayList<>();

        ranking.add(dru1);
        ranking.add(dru2);
        ranking.add(dru3);
        ranking.add(dru4);
        ranking.add(dru5);
        ranking.add(dru6);
    }

    @Override
    public String toString() {
        return "DruzynaPilkarska{" +
                "nazwa='" + nazwa + '\'' +
                ", pozycjaWRankingu=" + pozycjaWRankingu +
                ", silaDruzyny=" + silaDruzyny +
                '}';
    }

    public static class KomparatorDruzyn1 implements Comparator<DruzynaPilkarska>{

        @Override
        public int compare(DruzynaPilkarska o1, DruzynaPilkarska o2) {
            if(o1.pozycjaWRankingu > o2.pozycjaWRankingu){
                return 1;
            }else if(o1.pozycjaWRankingu < o2.pozycjaWRankingu){
                return -1;
            }
            return 0;
        }
    }

    public static class KomparatorDruzyn2 implements Comparator<DruzynaPilkarska>{

        @Override
        public int compare(DruzynaPilkarska o1, DruzynaPilkarska o2) {
            if(o1.silaDruzyny/o1.pozycjaWRankingu > o2.silaDruzyny/o2.pozycjaWRankingu){
                return 1;
            }else if (o1.silaDruzyny/o1.pozycjaWRankingu < o2.silaDruzyny/o2.pozycjaWRankingu){
                return -1;
            }
            return 0;
        }
    }
}
