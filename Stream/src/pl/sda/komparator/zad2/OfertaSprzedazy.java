package pl.sda.komparator.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OfertaSprzedazy {
    String nazwaProduktu;
    int cena;

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getNazwaProduktu() {

        return nazwaProduktu;
    }

    public int getCena() {
        return cena;
    }


    public OfertaSprzedazy(String nazwaProduktu, int cena) {
        this.nazwaProduktu = nazwaProduktu;
        this.cena = cena;
    }

    public static void main(String[] args) {
        OfertaSprzedazy o1 = new OfertaSprzedazy("pała bejsbolowa", 150);
        OfertaSprzedazy o2 = new OfertaSprzedazy("wycieraczka", 45);
        OfertaSprzedazy o3 = new OfertaSprzedazy("gąbka", 12);
        OfertaSprzedazy o4 = new OfertaSprzedazy("widelec", 15);
        OfertaSprzedazy o5 = new OfertaSprzedazy("komputer", 1540);

        List<OfertaSprzedazy> oferty = new ArrayList<>();

        oferty.add(o1);
        oferty.add(o2);
        oferty.add(o3);
        oferty.add(o4);
        oferty.add(o5);

        KomparatorOfert kompare = new KomparatorOfert(true);

        System.out.println(oferty);

        Collections.sort(oferty, kompare);

        System.out.println(oferty);

    }

    @Override
    public String toString() {
        return "OfertaSprzedazy{" +
                "nazwaProduktu='" + nazwaProduktu + '\'' +
                ", cena=" + cena +
                '}';
    }


    public static class KomparatorOfert implements Comparator<OfertaSprzedazy> {

        boolean kolejnosc;


        public KomparatorOfert(boolean kolejnosc) {
            this.kolejnosc = kolejnosc;
        }

        @Override
        public int compare(OfertaSprzedazy o1, OfertaSprzedazy o2) {

            if (kolejnosc == true) {
                if (o1.getCena() > o2.getCena()) {
                    return 1;
                } else if (o1.getCena() < o2.getCena()) {
                    return -1;
                }
                return 0;
            } else if (kolejnosc == false) {
                if (o1.getCena() > o2.getCena()) {
                    return -1;
                } else if (o1.getCena() < o2.getCena()) {
                    return 1;
                }
            }
                return 0;
        }
    }
}
