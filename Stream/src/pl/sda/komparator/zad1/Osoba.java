package pl.sda.komparator.zad1;

import java.util.*;

public class Osoba {
    String imie;
    int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    public static void main(String[] args) {

        List<Osoba> ludzie = new ArrayList<>();
        Osoba o1 = new Osoba("ania", 23);
        Osoba o2 = new Osoba("irek", 25);
        Osoba o3 = new Osoba("arleta", 30);
        Osoba o4 = new Osoba("arta", 14);
        Osoba o5 = new Osoba("aeta", 68);

        ludzie.add(o1);
        ludzie.add(o2);
        ludzie.add(o3);
        ludzie.add(o4);
        ludzie.add(o5);

        System.out.println(ludzie);

        ComparatorOsob comparator = new ComparatorOsob();

        Collections.sort(ludzie, comparator);
        System.out.println(ludzie);


    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", wiek=" + wiek +
                '}';
    }

    public static class ComparatorOsob implements Comparator<Osoba> {

        @Override
        public int compare(Osoba o1, Osoba o2) {
            if(o1.getWiek() > o2.getWiek()){
                return 1;
            }else if(o1.getWiek()< o2.getWiek()){
                return -1;
            }
            return 0;
        }
    }
}
