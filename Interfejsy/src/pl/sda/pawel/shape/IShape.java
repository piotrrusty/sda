package pl.sda.pawel.shape;

public interface IShape {

    public double calculateArea();
    public double calculateCircumference();
}
