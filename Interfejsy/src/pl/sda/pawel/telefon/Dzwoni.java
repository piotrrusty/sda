package pl.sda.pawel.telefon;

public interface Dzwoni {
    static String nrAlarmowy = "112";

    void zadzwon(String nrTel);
    void zadzwonNaNrAlarmowy();
}
