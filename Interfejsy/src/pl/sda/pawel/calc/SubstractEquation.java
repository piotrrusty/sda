package pl.sda.pawel.calc;

public class SubstractEquation implements ICalculable {
    double a = 23.456;
    double b = 746.95;

    public SubstractEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double sub = a-b;
        return sub;
    }
}
