package pl.sda.pawel.calc;

public interface ICalculable {
    public double calculate();
}
