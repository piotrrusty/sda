package pl.sda.pawel.calc;

public class PowEquation implements ICalculable {

    private double n;
    private double pot;

    public PowEquation(double n, double pot) {
        this.n = n;
        this.pot = pot;
    }

    @Override
    public double calculate() {
        double pow = Math.pow(n, pot);
        return pow;
    }
}
