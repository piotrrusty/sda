package pl.sda.pawel.calc;

import java.util.Scanner;

public class Main {
    static String dzialanie;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj dzialanie add/sub/div/mul/pow");
        dzialanie = input.nextLine();

        if(dzialanie.contains("add")) {
            System.out.println("Podaj pierwszą liczbę:");
            double a = input.nextDouble();
            System.out.println("Podaj drugą liczbę:");
            double b = input.nextDouble();

            SumEquation sum = new SumEquation(a, b);
            System.out.println("Wynik dodawania tych dwóch liczb to:");
            System.out.println(sum.calculate());

        }
        if(dzialanie.contains("sub")) {
            System.out.println("Podaj pierwszą liczbę:");
            double a = input.nextDouble();
            System.out.println("Podaj drugą liczbę:");
            double b = input.nextDouble();

            SubstractEquation sub = new SubstractEquation(a, b);
            System.out.println("Wynik odejmowania tych dwóch liczb to:");
            System.out.println(sub.calculate());

        }
        if(dzialanie.contains("div")) {
            System.out.println("Podaj pierwszą liczbę:");
            double a = input.nextDouble();
            System.out.println("Podaj drugą liczbę:");
            double b = input.nextDouble();

            DivEquation div = new DivEquation(a, b);
            System.out.println("Wynik dzielenia tych dwóch liczb to:");
            System.out.println(div.calculate());

        }
        if(dzialanie.contains("mul")) {
            System.out.println("Podaj pierwszą liczbę:");
            double a = input.nextDouble();
            System.out.println("Podaj drugą liczbę:");
            double b = input.nextDouble();

            MulEquation mul = new MulEquation(a, b);
            System.out.println("Wynik mnożeniadiv tych dwóch liczb to:");
            System.out.println(mul.calculate());

        }
        if(dzialanie.contains("pow")) {
            System.out.println("Podaj pierwszą liczbę:");
            double a = input.nextDouble();
            System.out.println("Podaj drugą liczbę:");
            double b = input.nextDouble();

            PowEquation pow = new PowEquation(a, b);
            System.out.println("Wynik potęgowania tych dwóch liczb to:");
            System.out.println(pow.calculate());

        }
    }
}
