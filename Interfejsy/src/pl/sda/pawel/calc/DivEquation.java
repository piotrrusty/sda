package pl.sda.pawel.calc;

public class DivEquation implements ICalculable {
    double a = 7652.87;
    double b = 46376.297;

    public DivEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double div = a/b;
        return div;
    }
}
