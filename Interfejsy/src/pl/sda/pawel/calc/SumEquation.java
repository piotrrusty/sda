package pl.sda.pawel.calc;

public class SumEquation implements ICalculable {
    private double a;
    private double b;

    public SumEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double sum = a +b;
        return sum;
    }
}
