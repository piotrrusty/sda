package pl.sda.pawel.calc;

public class MulEquation implements ICalculable {
    double a = 3652.87;
    double b = 36242.985;

    public MulEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        double mul = a*b;
        return mul;
    }
}
