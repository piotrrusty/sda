package pl.sda.pawel.family;

public class Father implements IFamilyMember{
    String name = "Johnny";

    @Override
    public void introduce() {
        System.out.println("I am your father");
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
