package pl.sda.pawel.family;

public class Daughter implements IFamilyMember {
    String name = "Ania";

    @Override
    public void introduce() {
        System.out.println("I am daughter");
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
