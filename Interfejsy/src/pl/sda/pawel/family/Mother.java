package pl.sda.pawel.family;

public class Mother implements IFamilyMember {
    String name;

    @Override
    public void introduce() {
        System.out.println("I am mother");
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
