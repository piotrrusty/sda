package pl.sda.pawel.family;

public interface IFamilyMember {

    void introduce();
    boolean isAdult();
}
