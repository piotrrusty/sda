package pl.sda.pawel.family;

public class Son implements IFamilyMember {
    String name = "Robert";

    @Override
    public void introduce() {
        System.out.println("Who's asking?");
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
