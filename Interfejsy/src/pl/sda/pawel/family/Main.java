package pl.sda.pawel.family;

public class Main {
    public static void main(String[] args) {
        IFamilyMember[] members = new IFamilyMember[4];
        members[0] = new Mother();
        members[1] = new Father();
        members[2] = new Daughter();
        members[3] = new Son();

        for(IFamilyMember member: members) {
            member.introduce();
        }
    }
}
