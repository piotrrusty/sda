package pl.sda.pawel.instrumenty;

public class Beben implements Instrumentalny {
    String dzwiek = "Bam Bam Bam";

    @Override
    public void graj() {
        System.out.println(dzwiek);
    }

    public static void main(String[] args) {
        Beben beben = new Beben();

        beben.graj();
    }
}
