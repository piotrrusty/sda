package pl.sda.dateandtime;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Zad3 {

    public static void main(String[] args) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        LocalDate date = LocalDate.now();
        LocalDate date2 = LocalDate.of(1876, 2, 14);

        date2.format(formatter);

        System.out.println(date2);

        Period period = Period.between(date, date2);
    }
}
