package pl.sda.dateandtime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zad7 {

    Period period;

    boolean czyPali;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        LocalDateTime poczatek = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        System.out.println("Podaj datę urodzenia w formacie: yyyy-MM-dd");
        LocalDate urodzenia = LocalDate.parse(input.nextLine(), formatter);

        System.out.println("Kobieta czy mężczyzna? Wpisz k/m");
        String plec = input.nextLine();

        System.out.println("Czy palisz papierosy? tak/nie");
        String czyPalisz = input.nextLine();

        System.out.println("Czy żyjesz w stresie? tak/nie");
        String stres = input.nextLine();

        int lata = 100;
        int miesiace = 0;
        int dni = 0;


        if (plec.toLowerCase().contains("m")) {
            lata -= 10;
        }
        if (czyPalisz.toLowerCase().contains("t")) {
            lata -= 9;
            miesiace -= 3;
        }
        if (stres.toLowerCase().contains("t")){
            lata -=7;
            miesiace -=6;
        }

        LocalDateTime koniec = LocalDateTime.now();

        Duration strata = Duration.between(poczatek, koniec);

        LocalDate dataSmierci = urodzenia.plusMonths(miesiace).plusYears(lata);
        System.out.println(dataSmierci);
        System.out.println("Na sprawdzaniu daty śmierci straciłeś "+ strata.getSeconds() + " sekund.");


    }
}
