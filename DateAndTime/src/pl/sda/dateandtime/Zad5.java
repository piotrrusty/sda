package pl.sda.dateandtime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zad5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");


        System.out.println("Podaj datę swojego urodzenia w formacie dd/mm/yyyy");
        LocalDate urodzenia = LocalDate.parse(input.nextLine(), formatter);
        LocalDate teraz = LocalDate.now();

        Period period = Period.between(urodzenia, teraz);

        System.out.println("Jesteś tak stary/stara:");
        System.out.println(periodWFormacie(period));


    }

    public static String periodWFormacie(Period p){
        return "Lat " + p.getYears() + " Miesięcy " + p.getMonths() + " Dni " + p.getDays();
    }
}
