package pl.sda.dateandtime;

import java.time.LocalDate;

public class Zad2 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();
        System.out.println(date.plusDays(10));
        System.out.println(date.minusDays(10));
    }
}
