package pl.sda.dateandtime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Scanner;

public class Zad1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj komende");
        String komenda = input.nextLine();

        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();

        LocalDateTime datetime = LocalDateTime.now();

        if(komenda.toLowerCase().contains("date")){
            System.out.println(date);
        }else if (komenda.toLowerCase().contains("time")){
            System.out.println(time);
        }else if (komenda.toLowerCase().contains("datetime")){
            System.out.println(datetime);
        }else if (komenda.toLowerCase().contains("quit")){
            System.exit(0);
        }
    }
}
