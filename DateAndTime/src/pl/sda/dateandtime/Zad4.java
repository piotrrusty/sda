package pl.sda.dateandtime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zad4 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        System.out.println("Podaj pierwszą datę w formcie DD/MM/YYYY");
        LocalDate date1 = LocalDate.parse(input.nextLine(), formatter);
        System.out.println("Podaj drugą datę w formacie DD/MM/YYYY");
        LocalDate date2 = LocalDate.parse(input.nextLine(), formatter);

        System.out.println(date1+ "    " + date2);

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        Period period = Period.between(date1, date2);
        System.out.println(formatPeriod(period));

    }

    public static String formatPeriod (Period p){
        return "Lat: " + p.getYears() + " Miesięcy: " + p.getMonths() + " Dni: "+ p.getDays();
    }
}
