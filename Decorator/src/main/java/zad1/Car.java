package zad1;

public class Car implements ICar {

    double chargerPressure;
    double engineCapcity;
    int seatsNumber;
    boolean hasCharger;
    double horsePower;

    public Car(double chargerPressure, double engineCapcity, int seatsNumber, boolean hasCharger, double horsePower) {
        this.chargerPressure = chargerPressure;
        this.engineCapcity = engineCapcity;
        this.seatsNumber = seatsNumber;
        this.hasCharger = hasCharger;
        this.horsePower = horsePower;
    }


    public double getHorsePower() {
        return horsePower;
    }

    public boolean hasCharger() {
        return hasCharger;
    }

    public double getEngineCapacity() {
        return engineCapcity;
    }

    public double getChargerPressure() {
        return chargerPressure;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }

    public void setChargerPressure(double chargerPressure) {

        this.chargerPressure = chargerPressure;
    }

    public void setEngineCapcity(double engineCapcity) {
        this.engineCapcity = engineCapcity;
    }
}
