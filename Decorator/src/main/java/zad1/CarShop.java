package zad1;

public class CarShop {


    public ICar addExtraCharger(Car car){

        TunedCar tunedCar = new TunedCar(car);

        car.setChargerPressure(car.getChargerPressure()*1.15);

        if (!tunedCar.hasCharger()){
            tunedCar. = true;
        }

        return tunedCar;
    }

    public ICar addExtraEngine(Car car) {

        TunedCar tunedCar = new TunedCar(car);

        car.setHorsePower(car.getHorsePower() * 1.20);
        car.setEngineCapcity(car.getEngineCapacity()*1.20);

        return tunedCar;
    }

    public
}
