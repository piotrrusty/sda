package zad1;

public class TunedCar implements ICar {

    private Car car;

    public TunedCar(Car car) {
        this.car = car;
    }

    public double getHorsePower() {
        double tunedHorsePower = car.getHorsePower() * 1.2;

        return tunedHorsePower;
    }

    public boolean hasCharger() {
        return car.hasCharger();
    }

    public double getEngineCapacity() {
        return getEngineCapacity();
    }

    public double getChargerPressure() {
        return getChargerPressure();
    }

}
