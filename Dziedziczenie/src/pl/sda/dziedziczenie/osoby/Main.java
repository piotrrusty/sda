package pl.sda.dziedziczenie.osoby;

public class Main {
    public static void main(String[] args) {
        Student jan = new Student("Jan", "Kowalski", 22, 12345);

        jan.przedstawSie();

        final Osoba paulina = new Osoba("Paulina", "Nowacka", 33);
        paulina.przedstawSie();
//        paulina.imie = "Anna"; nie skompiluje sie bo "imie" jest final
        paulina.przedstawSie();
        System.out.println(paulina);

        Osoba beata = new Osoba("Beata", "Lkaiajk", 22);

        beata = paulina;

//        paulina = beata; to się nie skompiluje bo final

        Student jacek = new Student("Jacek", "Nowak", 25, 12345);

        System.out.println(jan.equals(jacek));
    }
}
