package pl.sda.dziedziczenie.osoby;

public class PolimorfizmMain {

    public static void main(String[] args) {
        Osoba tomek = new Student("Tomasz", "Nowak", 23, 345678);

        tomek.przedstawSie();
        Student student = (Student) tomek;
        System.out.println(student.pokażIndeks());

    }
}
