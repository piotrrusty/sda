package pl.sda.dziedziczenie.samochody;

public class Kabriolet extends Samochod {

    boolean czyDachSchowany=false;

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    @Override
    public void przyspiesz() {
        this.predkosc += 10;
        if(this.predkosc<180) {
            System.out.println("Przyspieszam do " + this.predkosc + "km/h");
        }
        if(this.predkosc>=180) {
            System.out.println("Osiągnięto max prędkość");
        }
    }

    public void schowajDach(){

        if(!czyDachSchowany){
            this.czyDachSchowany = true;
            System.out.println("Dach został schowany");
        }else {
            System.out.println("Dach jest już schowany");
        }
    }

    public boolean CzyDachSchowany(){
        return czyDachSchowany;
    }

    @Override
    public String toString() {
        return super.toString()+" z rozsuwanym dachem";
    }
}
