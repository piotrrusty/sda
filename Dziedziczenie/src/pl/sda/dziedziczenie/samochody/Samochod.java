package pl.sda.dziedziczenie.samochody;

public class Samochod {

    protected int predkosc=0;
    protected boolean swiatla=false;
    String kolor;
    String marka;
    int rocznik;


    public Samochod(String kolor, String marka, int rocznik){
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz() {
        this.predkosc += 10;
        if(this.predkosc<120) {
            System.out.println("Przyspieszam do " + this.predkosc + "km/h");
        }
        if(this.predkosc>=120) {
            System.out.println("Osiągnięto max prędkość");
        }
    }

    public void wlaczSwiatla() {
        this.swiatla = true;
        System.out.println("Światła włączone");
    }

    public boolean czySwiatlaWlaczone(){
        return swiatla;
    }

    @Override
    public String toString() {
        return String.format("%s samochód marki %s, rocznik %d", kolor, marka, rocznik);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod){
            Samochod that = (Samochod) obj;
            if (this.kolor == that.kolor && this.marka== that.marka && this.rocznik==that.rocznik){
                return true;
            }
        }
        return false;
    }
}
